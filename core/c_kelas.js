const pool = require('./pool')

function Category(){};
Category.prototype = {
    find : function(user = null, callback){
        if(user){
            var field = Number.isInteger(user) ? 'id' : 'id';
        }

        var bind = [];
        bind.push(user);

        let sql = `Select * from category where ${field} = $1 and flag = 'Y'`;

        pool.query(sql, bind, function(err, result){
            if(err) throw err;
            callback(result.rows[0]);
        });
    },
    
    findAll : function(flag = null, callback){
        let sql = `Select cat.*, usr.fullname from category cat left join users usr on usr.id = cat.created_who 
        where cat.flag = $1`;
        
        var bind = [];
        bind.push(flag);

        pool.query(sql, bind, function(err, result){
            if(err) throw err;  
            callback(result.rows);
        });
    },

    // settingDoc : function(flag = null, callback){
    //     let sql = `select
    //                 div.id, div.nama_divisi,
    //                 akses.id as akses_id,
    //                 akses.id_divisi as akses_id_divisi,
    //                 akses.id_divisi_doc as akses_id_divisi_doc,
    //                 CASE WHEN akses.flag is null then 'N' else akses.flag end as flag,
    //                 CASE 
    //                     WHEN (akses.id_divisi is null) THEN '-' 
    //                     WHEN (akses.flag = 'N') THEN '-' 
    //                     ELSE 'checked' 
    //                 END as akses
    //             from 	
    //                 divisi div
    //             left join akses_doc akses on akses.id_divisi_doc = div.id and akses.id_divisi = $1
    //             where
    //                 div.flag = 'Y'`;
        
    //     var bind = [];
    //     bind.push(flag);

    //     pool.query(sql, bind, function(err, result){
    //         if(err) throw err;  
    //         callback(result.rows);
    //     });
    // },

    create : function(body, callback)
    {
        var bind = [];
        
        for(prop of Object.values(body)){
            bind.push(prop)
        }

        let sql = `INSERT INTO category(category_name, category_keterangan, flag, created_who, last_update_who, 
            created_date, last_update) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id`;

        pool.query(sql, bind, function(err, lastId){
            if(err){
                console.log(err);
                throw err;
            } 
            callback(lastId);
        });
    },

    delete : function(body, callback)
    {
        var bind = [];
        
        for(prop of Object.values(body)){
            bind.push(prop)
        }

        console.log(bind)
        let sql = `update category set flag = $1, last_update_who = $2, last_update = $3 where id = $4`;

        pool.query(sql, bind, function(err, lastId){
            if(err){
                console.log(err);
                throw err;
            } 
            callback(lastId);
        });
    },

    update : function(body, callback)
    {
        var bind = [];
        
        for(prop of Object.values(body)){
            bind.push(prop)
        }

        console.log(bind)
        let sql = `update category set category_name = $1, category_keterangan = $2, last_update = $3, 
        last_update_who = $4 where id = $5`;

        pool.query(sql, bind, function(err, lastId){
            if(err){
                console.log(err);
                throw err;
            } 
            callback(lastId);
        });
        
    },

}

module.exports = Category;