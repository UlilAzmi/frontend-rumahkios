const util = require('util');
const { Pool } = require('pg')

// Connection to the Database
const pool = new Pool({
    user: 'postgres',
    password: '123_Terserah',
    host: 'localhost',
    database: 'template',
    port : 5432
});

pool.connect((err, connnection) => {
    if(err){
        console.error("Something went wrong connecting to the database ...");
    }

    if(connnection){
        connnection.release();
    }
    return;
});

pool.query = util.promisify(pool.query);

module.exports = pool;
