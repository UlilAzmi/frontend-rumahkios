const pool = require('./pool')

function c_home(){};
c_home.prototype = {
    // find : function(user = null, callback){
    //     if(user){
    //         var field = Number.isInteger(user) ? 'id' : 'id';
    //     }

    //     var bind = [];
    //     bind.push(user);

    //     let sql = `Select * from users where ${field} = $1 and flag = 'Y'`;

    //     pool.query(sql, bind, function(err, result){
    //         if(err) throw err;
    //         callback(result.rows[0]);
    //     });
    // },
    
    getDataCount : function(flag = null, callback){
        let sql = `select 
                    count(doc.nama_document),
                    div.nama_divisi
                from 
                    document doc
                left join users usr on usr.id = doc.created_who
                left join jabatan jab on jab.id = usr.id_position
                left join divisi div on div.id = jab.id_divisi
                where
                    doc.flag = $1
                group by div.nama_divisi`;
        
        var bind = [];
        bind.push(flag);

        pool.query(sql, bind, function(err, result){
            if(err) throw err;  
            callback(result.rows);
        });
    }
}

module.exports = c_home;