const pool = require('./pool')
const bcrypt = require('bcrypt')

function User(){};
User.prototype = {
    find : function(user = null, callback){
        // if user = number return field = id, if user = string return field = username. 
        if(user){
            var field = Number.isInteger(user) ? 'id' : 'username';
        }

        var bind = [];
        bind.push(user);

        let sql = `select usr.*,jab.id_divisi, jab.nama_jabatan from users usr left join jabatan jab on jab.id = usr.id_position 
        where usr.flag = 'Y' and ${field} = $1`;

        pool.query(sql, bind, function(err, result){
            if(err) throw err;
            callback(result.rows[0]);
        });
    },

    getMenu : function(user = null, callback){
        // if user = number return field = id, if user = string return field = username. 
        var bind = [];
        bind.push(user);

        let sql = `select 
                        m.nama,
                        m.url 
                    from 
                        priveleges p
                    left join menu m on m.id = p.id_menu
                    where p.id_position = $1 and p.flag = 'Y'`;
        pool.query(sql, bind, function(err, result){
            if(err) throw err;
            callback(result.rows);
        });
    },

    getAksesDoc : function(divisi = null, callback){
        var bind = [];
        bind.push(divisi);

        let sql = `select akses.id, akses.id_divisi, div.nama_divisi, akses.id_divisi_doc 
                from 
                    akses_doc akses 
                    left join divisi div on div.id = akses.id_divisi_doc
                where 
                    akses.flag = 'Y' 
                    and akses.id_divisi = $1`;
        pool.query(sql, bind, function(err, result){
            if(err) throw err;
            callback(result.rows);
        });
    },
    
    create : function(body, callback)
    {
        let pwd = body.password;
        body.password = bcrypt.hashSync(pwd, 10);

        var bind = [];
        
        for(prop of Object.values(body)){
            console.log(prop)
            bind.push(prop)
        }

        let sql = `INSERT INTO users(username, fullname, password) VALUES ($1, $2, $3)`;

        console.log(sql)
        pool.query(sql, bind, function(err, lastId){
            if(err){
                console.log(err);
                throw err;
            } 
            callback(lastId);
        });
    },

    login : function(username, password, callback)
    {
        this.find(username, function(user){
            if(user){
                if(bcrypt.compareSync(password, user.password)){
                    callback(user);
                    return;
                }
            }
            callback(null);
        });
    },
    
}

module.exports = User;