# RumahKios Frontend
This service responsibility as Frontend service

## Build an Image for staging
If use the default docker image registry

### `make build-image-staging`

If use custom docker image registry

### `RUMAHKIOS_FE_API_REGISTRY=$URL_REGISTRY make build-image-registry`

## Push docker Image to registry

If use the default docker registry

### `make push-image-registry`

If use custom docker image registry

### `RUMAHKIOS_FE_API_REGISTRY=$URL_REGISTRY make push-image-registry`

## Build all(staging) until push image to registry

If use the default docker registry
### `make build-all-staging`

If use custom docker image registry
### `RUMAHKIOS_FE_API_REGISTRY=$URL_REGISTRY make build-all-staging`

## Production coming soon

