const express = require('express');
const utilSim = require('../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('premium', { title: "Member Premium"});
});

// router.get('/all',utilSim.checkUserSession, (req, res, next) => {
//     utilSim.httpSimGet('/systems/tahunAjaran', req, res, function(result){
//         res.json({success :true, data: result.data})
//     })
// });

// router.get('/bulan',utilSim.checkUserSession, (req, res, next) => {
//     console.log("okkokookokoko")
//     utilSim.httpSimGet('/systems/tahunAjaran/bulan/' + req.query.id, req, res, function(result){
//         res.json({success :true, data: result.data})
//     })
// });

// router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
//     utilSim.httpSimGet('/systems/tahunAjaran/' + req.query.id, req, res, function(result){
//         res.json({success :true, data: result.data[0]})
//     })
// });

// router.post('/add', utilSim.checkUserSession, (req, res, next) => {
//     req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
//     req.check('tanggalAwal', 'invalid tanggalAwal').not().isEmpty().withMessage('tanggal awal is required');
//     req.check('tanggalAkhir', 'invalid tanggalAkhir').not().isEmpty().withMessage('tanggal akhir is required');
//     req.check('semester', 'invalid semester').not().isEmpty().withMessage('semester is required');
//     req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');
    
//     var errors = req.validationErrors();
//     if(errors){
//         res.json({errors: errors})
//     }else{
//         let userInput = JSON.stringify({
//             nama: req.body.nama,
//             tanggalAwal: req.body.tanggalAwal,
//             tanggalAkhir: req.body.tanggalAkhir,
//             semester: req.body.semester,
//             status : 'N',
//             keterangan: req.body.keterangan,
//         });
        
//         utilSim.httpSimPost('/systems/tahunAjaran', userInput, req, function(result){
//             if(result.rc == "00"){
//                 res.json({errors: errors, success :true, message: result.rd})
//             }else{
//                 res.json({errors: errors, success :false, message: result.rd})
//             }
//         });
//     } 
// });

// router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
//     req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')
//     console.log(req.body.id)
//     var errors = req.validationErrors();
//     if(errors){
//         res.json({errors: errors})
//     }else{
//         utilSim.httpSimDelete('/systems/tahunAjaran/' + req.body.id, req, function(result){
//             console.log(result)
//             if(result.rc == "00"){
//                 res.json({errors: errors, success :true, message: result.rd})
//             }else{
//                 res.json({errors: errors, success :false, message: result.rd})
//             }
//         });
//     }
// });

// router.post('/update', utilSim.checkUserSession, (req, res, next) => {
//     req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
//     req.check('tanggalAwal', 'invalid tanggalAwal').not().isEmpty().withMessage('tanggal awal is required');
//     req.check('tanggalAkhir', 'invalid tanggalAkhir').not().isEmpty().withMessage('tanggal akhir is required');
//     req.check('semester', 'invalid semester').not().isEmpty().withMessage('semester is required');
//     req.check('status', 'invalid status').not().isEmpty().withMessage('status is required');
//     req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');

//     var errors = req.validationErrors();
//     if(errors){
//         res.json({errors: errors})
//     }else{
//         let userInput = JSON.stringify({
//             id: req.body.id,
//             nama: req.body.nama,
//             tanggalAwal: req.body.tanggalAwal,
//             tanggalAkhir: req.body.tanggalAkhir,
//             semester: req.body.semester,
//             status : req.body.status,
//             keterangan: req.body.keterangan,
//         });

//         utilSim.httpSimPut('/systems/tahunAjaran/', userInput, req, function(result){
//             console.log(result)
//             if(result.rc == "00"){
//                 res.json({errors: errors, success :true, message: result.rd})
//             }else{
//                 res.json({errors: errors, success :false, message: result.rd})
//             }
//         });
//     } 
// });

module.exports = router;