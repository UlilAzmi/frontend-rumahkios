const express = require('express');
// const User = require('../core/user');
const utilSim = require('../core/utilSim');
var http = require('http');
const router = express.Router();

// const user = new User();
router.get('/login', (req, res, next) => {
        res.render('login', {
            title: "Login", 
            layout: false
        })
});

router.get('/register', (req, res, next) => {
    res.render('register', {
        title: "Register New Member", 
        layout: false
    })
});

router.get('/', (req, res, next) => {
    let env = req.query.env;
    let idDevel = req.query.idDevel;
    if(undefined == req.query.env){
        env = 'prod';
    }

    if(undefined == req.query.idDevel){
        idDevel = 0;
    }
    utilSim.httpSimGet('/homepage/?env='+env+'&idDevel='+idDevel, req, res, function(result){
        var isLogin = false;
        
        if(req.session.jwt != undefined){
            isLogin = true;
        }
        
        if(env == 'devel'){
            res.render('indexDevel', {title: "Home Page",
            flashSale: result.data.flashSale, product: result.data.listProduk,  env: env, idDevel: idDevel,
            success: req.session.success, errors: req.session.errors, layout: "mainDevel"})
        }else{
            res.render('index', {title: "Home Page", carousel: result.data.carousel, carouselMini: result.data.carouselMini, 
            flashSale: result.data.flashSale, product: result.data.listProduk, footAbout: result.data.footer.about, 
            footFb: result.data.footer.facebook, footTwit: result.data.footer.twitter, footIg: result.data.footer.instagram, footGoogle: result.data.footer.googlePlus, 
            footAlamat: result.data.footer.alamat,
            success: req.session.success, errors: req.session.errors, isLogin: isLogin})
        }

        // res.json({success :true, data: result.data})
    })
});

router.get('/about', (req, res) => {
    res.render('admin/about', {title: "About Me"})
});

// router.get('/user',checkUserSession, (req, res) => {
//     res.render('user', {title: "User"})
// });

router.get('/data',checkUserSession, (req, res) => {
    res.render('data', {title: "Data"})
});

//Post login data
router.post('/submit', (req, res, next) => {
    req.check('username', 'invalid username').not().isEmpty().withMessage('Name is required')
    req.check('password', 'invalid password').not().isEmpty().withMessage('password is required');

    var errors = req.validationErrors();
    if(errors){
        req.session.errors = errors;
        req.session.success = false;
        res.render('login', {
            title: "Login", 
            layout: false,
            success: req.session.success, 
            errors: req.session.errors
        })
    }else{
        var data = JSON.stringify({
            nis: req.body.username,
            password: req.body.password,
          });
        
        utilSim.httpLogin('/authenticate', data, req, res);
    }
});

// //Post login data
router.post('/submit2', (req, res, next) => {
    req.check('username', 'invalid username').not().isEmpty().withMessage('Name is required');
    req.check('email', 'invalid username').not().isEmpty().withMessage('Email');
    req.check('notelp', 'invalid username').not().isEmpty().withMessage('No telp');
    req.check('password1', 'invalid password').not().isEmpty().withMessage('password is required');

    var errors = req.validationErrors();
    if(errors){
        console.log(errors)
        req.session.errors = errors;
        req.session.success = false;
        res.render('register', {
            title: "Register", 
            layout: false,
            success: req.session.success, 
            errors: req.session.errors,
            username: req.body.username,
            email: req.body.email,
            notelp: req.body.notelp,
            password1: req.body.password1
        })
    }else{

        var data = JSON.stringify({
            name: req.body.username,
            password: req.body.password1,
            email: req.body.email,
            noTelp: req.body.notelp,
          });
        utilSim.httpSimPost('/register', data, req, function(result){
            if(result.rc == "00"){
                res.render('register', {
                    title: "Register", 
                    layout: false,
                    berhasil: true
                })
            }else{
                console.log(result)
                res.render('register', {
                    title: "Register", 
                    layout: false,
                    berhasil: false,
                    errors: [{msg : result.rd}],
                    username: req.body.username,
                    email: req.body.email,
                    notelp: req.body.notelp,
                    password1: req.body.password1
                })
                //res.json({errors: true, success :false, message: result.rd})
            }
        });
    }
});

function checkUserSession( req, res, next )
{
    if( req.session.user )
    {
        next();
    }
    else
    {
        res.redirect('/login');
    }
}

//Post register data
router.post('/register', (req, res, next) => {
    let userInput = {
        username: req.body.username,
        fullname: req.body.fullname,
        password: req.body.password
    };

    user.create(userInput, function(lastId){
        if(lastId){
            user.find(lastId, function(result){
                req.session.user = result;
                req.session.app = 0;
                res.redirect('/home');
            });
            res.send('Welcome ' + userInput.username);
        }else{
            console.log('Error creating a new user ...');
        }
    });
});

//Get Logout
router.get('/logout', (req, res, next) =>{
    if(req.session.user){
        req.session.destroy(function(){
            res.redirect('/');
        });
    }
});

module.exports = router;