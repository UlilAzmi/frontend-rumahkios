const express = require('express');
const utilSim = require('../core/utilSim');
const e = require('express');
const router = express.Router();


router.get('/', (req, res, next) => {
    let idDevel = req.query.idDevel;
    let env = req.query.env;
    
    if(undefined == req.query.env){
        env = 'prod';
    }

    if(undefined == req.query.idDevel){
        idDevel = 0;
    }

    utilSim.httpSimGet('/homepage/detail/' + req.query.id, req, res, function(result){
        var isLogin = false;
        if(req.session.jwt != undefined){
            isLogin = true;
        }
        var untilBuyDate = result.data.product.untilBuyDate;
        let num = '';
	if(isLogin){
		num = new Number(result.data.product.hargaFinal).toLocaleString("id-ID");
	}else{
		num = result.data.product.hargaHidden;
	}

        var timeDate = untilBuyDate.split("-")[2] + "-" + untilBuyDate.split("-")[1] + "-" + untilBuyDate.split("-")[0];
        
        if(env == 'devel'){
            res.render('produkDetail', { title: "Detail Produk", iklan: result.data.iklan, tanggal: timeDate, 
                time: result.data.product.untilBuyTime, product: result.data.product, harga: num, env: env, idDevel: idDevel,
                listProduct: result.data.listProduk, layout: "mainDevel"
            });
        }else{
            res.render('produkDetail', { title: "Detail Produk", iklan: result.data.iklan,
                footAbout: result.data.footer.about, footFb: result.data.footer.facebook, footTwit: result.data.footer.twitter, 
                footIg: result.data.footer.instagram, footGoogle: result.data.footer.googlePlus, footAlamat: result.data.footer.alamat, isLogin: isLogin,
                tanggal: timeDate, time: result.data.product.untilBuyTime, product: result.data.product, harga: num, hargaHidden: result.data.product.hargaHidden,
                listProduct: result.data.listProduk
            });
        }
    });
});

router.get('/product', (req, res, next) => {
    let idDevel = req.query.idDevel;
    let env = req.query.env;
    
    if(undefined == req.query.env){
        env = 'prod';
    }

    if(undefined == req.query.idDevel){
        idDevel = 0;
    }

    utilSim.httpSimGet('/homepage/product?idProduk=' + req.query.id + '&idDevel=' + idDevel + '&env=' + env, req, res, function(result){
        if(result.rc == '00'){
            res.json({success :true, data: result.product})
        }else{
            res.json({success :false, errors: result.rd})
        }
    })
});


module.exports = router;
