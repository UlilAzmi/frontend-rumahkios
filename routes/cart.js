const express = require('express');
const utilSim = require('../core/utilSim');
const { use } = require('../app');
const router = express.Router();


router.post('/', utilSim.checkUserSession, (req, res, next) => {
    let userInput = JSON.stringify({
        idProduk: parseInt(req.body.id)
    });
    
    let env = req.body.env;

    if(undefined == env || null == env){
        env = 'prod';
    }

    if(env == 'devel'){
        utilSim.httpSimPostDevel('/systems/cart', userInput, req, function(result){
            if(result.rc == "00"){
                // res.render('admin/cart', { title: "My Cart", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    }else{
        utilSim.httpSimPost('/systems/cart', userInput, req, function(result){
            if(result.rc == "00"){
                // res.render('admin/cart', { title: "My Cart", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    }
});

router.get('/myCart', utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        let menu = utilSim.getMenuDevel(); 
        let user = utilSim.getUserDevel();
        
        res.render('admin/cart', { title: "My Cart", menu: JSON.parse(menu), user: JSON.parse(user), layout: "admin", isAdmin: false, env: "devel"});
    }else{
        res.render('admin/cart', { title: "My Cart", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin", isAdmin: false});
    }
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/cart/' + req.query.id, req, res, function(result){
            res.json({success :true, data: result.data[0]})
        })
    }else{
        utilSim.httpSimGet('/systems/cart/' + req.query.id, req, res, function(result){
            res.json({success :true, data: result.data[0]})
        })
    }
});

router.get('/data',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/cart', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }else{
        utilSim.httpSimGet('/systems/cart', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }
});

module.exports = router;