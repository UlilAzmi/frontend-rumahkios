const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/mutasiSaldo', {data:null, title: "Mutasi Saldo", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/mutasiSaldo?type='+req.query.type+'&filter=&page='+req.query.page+'&limit='+req.query.limit+'&idDebet=' + req.query.idDebet + 
    '&idCredit=' + req.query.idCredit + '&transaksiId=' + req.query.transaksiId, req, res, function(result){
        let jumlahPage = Math.round(result.total / req.query.limit)
        res.json({success :true, data: result.data, jumlahPage: jumlahPage, jumlahData : result.total})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/mutasiSaldo/' + req.query.id, req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, data: result.rd})
        }
    })
});

module.exports = router;