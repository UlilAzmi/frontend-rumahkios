const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/user', {title: "Data User terdaftar", 
    menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/users', req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, message: result.rd}) 
        }
    })
});

router.post('/alls',utilSim.checkUserSession, (req, res, next) => {
    var filter='';
    if(null != req.body.id && '' != req.body.id){
        filter = '?role='+ req.body.id;
    }
    utilSim.httpSimGet('/systems/users'+filter, req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, message: result.rd}) 
        }
    })
});

router.get('/all/wali',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/users?role=57', req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, message: result.rd}) 
        }
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/users/' + req.query.id, req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, message: result.rd}) 
        }
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('firstName', 'invalid First Name').not().isEmpty().withMessage('First Name is required')
    req.check('lastName', 'invalid Last Name').not().isEmpty().withMessage('Last Name is required')
    req.check('role', 'invalid role').not().isEmpty().withMessage('role is required');
    req.check('noTelp', 'invalid noTelp').not().isEmpty().withMessage('noTelp is required');
    req.check('email', 'invalid email').not().isEmpty().isEmail().withMessage('email is invalid');
    req.check('gender', 'invalid email').not().isEmpty().withMessage('gender is required');
    req.check('tempatLahir', 'invalid tempat lahir').not().isEmpty().withMessage('tempat lahir is required');
    req.check('tanggalLahir', 'invalid tanggal lahir').not().isEmpty().withMessage('tanggal lahir is required');
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            userName: req.body.firstName,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            noTelp: req.body.noTelp,
            alamat: req.body.alamat,
            gender: req.body.gender,
            tempatLahir: req.body.tempatLahir,
            tanggalLahir: req.body.tanggalLahir,
            password: '123123',
            matchingPassword: '123123',
            role: {id : parseInt(req.body.role)},
            kelas: {id : parseInt(req.body.kelas)},
        });
        utilSim.httpSimPost('/systems/users', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/users/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: result.rd, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('firstName', 'invalid First Name').not().isEmpty().withMessage('Nama harus diisi')
    req.check('role', 'invalid role').not().isEmpty().withMessage('role harus diisi');
    req.check('noTelp', 'invalid noTelp').not().isEmpty().withMessage('noTelp harus diisi');
    req.check('email', 'invalid email').not().isEmpty().isEmail().withMessage('email harus diisi');
    req.check('gender', 'invalid gender').not().isEmpty().withMessage('gender harus diisi');
    req.check('alamat', 'invalid alamat').not().isEmpty().withMessage('Alamat harus diisi');
    req.check('tempatLahir', 'invalid tempat lahir').not().isEmpty().withMessage('tempat lahir harus diisi');
    req.check('tanggalLahir', 'invalid tanggal lahir').not().isEmpty().withMessage('tanggal lahir harus diisi');
    
    if('4' == req.body.role){
        req.check('provinsiId', 'invalid provinsi').not().isEmpty().withMessage('Provinsi harus diisi');
        req.check('kotaId', 'invalid kota').not().isEmpty().withMessage('Kota harus diisi');
        req.check('kecamatanId', 'invalid kecamatan').not().isEmpty().withMessage('Kecamatan harus diisi');
        req.check('domain1', 'invalid domain1').not().isEmpty().withMessage('domain1 harus diisi');
        req.check('domain2', 'invalid domain2').not().isEmpty().withMessage('domain2 harus diisi');
        req.check('domain3', 'invalid domain3').not().isEmpty().withMessage('domain3 harus diisi');
    }

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : parseInt(req.body.id),
            name: req.body.firstName,
            email: req.body.email,
            noTelp: req.body.noTelp,
            alamat: req.body.alamat,
            gender: req.body.gender,
            tempatLahir: req.body.tempatLahir,
            tanggalLahir: req.body.tanggalLahir,
            roleId: parseInt(req.body.role),
            provinsiId: req.body.provinsiId,
            kotaId: req.body.kotaId,
            kecamatanId: req.body.kecamatanId,
            domain1: req.body.domain1,
            domain2: req.body.domain2,
            domain3: req.body.domain3,
        });
        utilSim.httpSimPut('/systems/users/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/password', utilSim.checkUserSession, (req, res, next) => {
    req.check('passwordLama', 'invalid First Name').not().isEmpty().withMessage('PasswordLama harus diisi')
    req.check('passwordBaru', 'invalid role').not().isEmpty().withMessage('PasswordBaru harus diisi');
    req.check('passwordBaru2', 'invalid noTelp').not().isEmpty().withMessage('PasswordBaru2 harus diisi');
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.nis,
            passwordLama: req.body.passwordLama,
            passwordBaru: req.body.passwordBaru,
            passwordBaru2: req.body.passwordBaru2
        });
        utilSim.httpSimPut('/systems/users/password', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;