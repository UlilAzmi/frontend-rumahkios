const express = require('express');
const utilSim = require('../../core/utilSim');
const e = require('express');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/tagihan', {data:null, title: "Tagihan Merchant", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    // let lunas = '&lunas=' + req.query.lunas;
    // let via = req.query.via;
    // let tanggal = req.query.tanggal;
    // let tanggalBeli = req.query.tanggalBeli;
    
    // if(req.query.lunas == undefined){
    //     lunas = '';
    // }
    // if(via == undefined){
    //     via = null;
    // }
    // if(tanggal == undefined){
    //     tanggal = null;
    // }
    // if(tanggalBeli == undefined){
    //     tanggalBeli = null;
    // }

    utilSim.httpSimGet('/systems/tagihan', req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

router.get('/myTransaksi1',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/transaksi/myTransaksi', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }else{
        utilSim.httpSimGet('/systems/transaksi/myTransaksi', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/' + req.query.id, req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, data: result.rd})
        }
    })
});

router.get('/lastCreate',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/tagihan/lastCreate', req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, data: result.rd})
        }
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('bulan', 'invalid bulan').not().isEmpty().withMessage('Pilih Bulan Tagihan')
    req.check('tahun', 'invalid tahun').not().isEmpty().withMessage('Pilih Tahun Tagihan')
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            bulan: parseInt(req.body.bulan),
            tahun: parseInt(req.body.tahun),
        });
        
        utilSim.httpSimPost('/systems/tagihan', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;