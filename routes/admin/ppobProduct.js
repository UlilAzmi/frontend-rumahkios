const express = require('express');
const utilSim = require('../../core/utilSim');
const multer = require('multer');
const fs = require('fs');

const router = express.Router();

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/datas/ppobCategory");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});

const upload = multer({
    storage: Storage,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('only .jpeg .jpg .png file type'))
        }
        
        cb(undefined, true)
    },
}).single("file");

router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/ppobProduct', { title: "PPOB Product", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/ppob/product/', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/ppob/product/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    let userInput = JSON.stringify({
        id: req.body.id,
        groupId: req.body.group,
        nama: req.body.nama,
        keterangan: req.body.keterangan,
        beli: req.body.beli,
        jual: req.body.jual,
        upHarga: req.body.hargaUp,
        productBillerId: req.body.productBillerId
    });
    utilSim.httpSimPost('/ppob/product/', userInput, req, function(result){
        if(result.rc == "00"){
            res.json({errors: false, success :true, message: result.rd})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
    });
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/ppob/product/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    userInput = JSON.stringify({
        id: req.body.id,
        groupId: req.body.group,
        nama: req.body.nama,
        keterangan: req.body.keterangan,
        beli: req.body.beli,
        jual: req.body.jual,
        upHarga: req.body.hargaUp,
        productBillerId: req.body.productBillerId
    });
    utilSim.httpSimPut('/ppob/product/', userInput, req, function(result){
        if(result.rc == "00"){
            res.json({errors: false, success :true, message: result.rd})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
    });

});

module.exports = router;