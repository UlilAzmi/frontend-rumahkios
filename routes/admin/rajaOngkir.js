const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();

router.get('/provinsi',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/ongkir/provinsi', req, res, function(result){
            res.json({success :true, data: result})
        })
    }else{
        utilSim.httpSimGet('/systems/ongkir/provinsi', req, res, function(result){
            res.json({success :true, data: result})
        })
    }
});

router.get('/kota',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/ongkir/kota?idProvinsi=' + req.query.id, req, res, function(result){
            res.json({success :true, data: result})
        })
    }else{
        utilSim.httpSimGet('/systems/ongkir/kota?idProvinsi=' + req.query.id, req, res, function(result){
            res.json({success :true, data: result})
        })
    }
});

router.get('/kecamatan',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/ongkir/kecamatan?idKota=' + req.query.id, req, res, function(result){
            res.json({success :true, data: result})
        })
    }else{
        utilSim.httpSimGet('/systems/ongkir/kecamatan?idKota=' + req.query.id, req, res, function(result){
            res.json({success :true, data: result})
        })
    }
});

router.get('/ongkir',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/ongkir?origin='+req.query.origin+'&destination='+req.query.destination+'&weight=' + req.query.weight, req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }else{
        utilSim.httpSimGet('/systems/ongkir?origin='+req.query.origin+'&destination='+req.query.destination+'&weight=' + req.query.weight, req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }
});

module.exports = router;