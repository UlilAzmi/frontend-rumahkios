const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/koperasiAnggota', { title: "Koperasi Anggota", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/koperasi/anggota', req, res, function(result){
        console.log(result.data)
        res.json({success :true, data: result.data})
    })
});


router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            userId : parseInt(req.body.userId),
            status: req.body.status
        });
        
        utilSim.httpSimPut('/systems/koperasi/status/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;