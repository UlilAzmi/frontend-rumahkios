
const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/productKoperasi', {title: "Product Koperasi", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/productKoperasi', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/productKoperasi/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name Harus diisi')
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let arrDetail = [];
        for(i = 0; i < req.body.produkDetail.length; i++){
            arrDetail[i] = parseInt(req.body.produkDetail[i])
        }
        let userInput = JSON.stringify({
            nama: req.body.nama,
            keterangan: req.body.keterangan,
            productKoperasiDetail: arrDetail
        });
        
        utilSim.httpSimPost('/systems/productKoperasi', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/productKoperasi/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Nama tidak boleh kosong')
    
    let arrDetail = [];
    for(i = 0; i < req.body.produkDetail.length; i++){
        arrDetail[i] = parseInt(req.body.produkDetail[i])
    }
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            nama: req.body.nama,
            keterangan: req.body.keterangan,
            productKoperasiDetail: arrDetail,
        });

        utilSim.httpSimPut('/systems/productKoperasi/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

module.exports = router;