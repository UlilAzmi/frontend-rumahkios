const express = require('express');
const utilSim = require('../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('jurusan', { title: "Jurusan", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user)});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/jurusan', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/jurusan/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            nama: req.body.nama,
            keterangan: req.body.keterangan,
        });
        
        utilSim.httpSimPost('/systems/jurusan', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    console.log(req.body.id)
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/jurusan/' + req.body.id, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    console.log('masuk')
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            nama: req.body.nama,
            keterangan: req.body.keterangan
        });

        utilSim.httpSimPut('/systems/jurusan/', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;