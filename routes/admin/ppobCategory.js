const express = require('express');
const utilSim = require('../../core/utilSim');
const multer = require('multer');
const fs = require('fs');

const router = express.Router();

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/datas/ppobCategory");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});

const upload = multer({
    storage: Storage,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('only .jpeg .jpg .png file type'))
        }
        
        cb(undefined, true)
    },
}).single("file");

router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/ppobCategory', { title: "PPOB Category Product", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/ppob/product/category', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/ppob/product/category/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    
    upload(req, res, function(err) {
        if (err) {
            console.log('Error')
            return res.send({ success :false, message: err.message});
        }

        let userInput = JSON.stringify({
            id: req.body.id,
            nama: req.body.nama,
            image: req.file.path,
            urutan: req.body.urutan
        });
        
        utilSim.httpSimPost('/ppob/product/category', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    })
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/ppob/product/category/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                fs.unlink(req.body.path, err => { if (err) console.log(err) });
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    upload(req, res, function(err) {
        let userInput;
        if(req.file !== undefined){
            
            fs.unlink(req.body.pathLama, err => { if (err) console.log(err) });

            userInput = JSON.stringify({
                id: req.body.id,
                nama: req.body.nama,
                image: req.file.path,
                urutan: req.body.urutan
            });
        }else{
            userInput = JSON.stringify({
                id: req.body.id,
                nama: req.body.nama,
                image: req.body.pathLama,
                urutan: req.body.urutan
            });
        }
        
        utilSim.httpSimPut('/ppob/product/category', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
        
    })
});

module.exports = router;