const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/approveBank', { title: "Approve Bank", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/approveBank?page='+req.query.page+"&limit="+req.query.limit, req, res, function(result){
        let jumlahPage = Math.round(result.total / req.query.limit)
        res.json({success :true, data: result.data, jumlahData: jumlahPage})
    })
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('pesanKeMerchant', 'invalid Primary Key 2').not().isEmpty().withMessage('Pesan harus diisi')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            statusApprove: req.body.statusApprove,
            pesan: req.body.pesanKeMerchant
        });

        utilSim.httpSimPut('/systems/approveBank/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;