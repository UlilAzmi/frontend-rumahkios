const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/metodeBayar', {title: "Metode Bayar", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout:'admin'});
}); 

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/metodeBayar', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/rk',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/metodeBayar/type/1', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/merchant',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/metodeBayar/type=2', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/metodeBayar/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {

    req.check('pg', 'invalid pg').not().isEmpty().withMessage('Payment Gateway harus diisi')
    req.check('bankId', 'invalid bankId').not().isEmpty().withMessage('ID Bank harus diisi');
    req.check('bankName', 'invalid bankName').not().isEmpty().withMessage('Nama Bank Harus diisi');
    req.check('logo', 'invalid logo').not().isEmpty().withMessage('URL Logo harus diisi');
    req.check('deskripsi', 'invalid deskripsi').not().isEmpty().withMessage('deskripsi harus diisi');
    req.check('stepBayar', 'invalid stepBayar').not().isEmpty().withMessage('Langlah pembayaran harus diisi');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({            
            idPg: req.body.pg,
            idBank: req.body.bankId,
            namaBank: req.body.bankName,
            deskripsi: req.body.deskripsi,
            logo: req.body.logo,
            forRk: parseInt(req.body.allowRk),
            forDineIn: parseInt(req.body.allowDineIn),
            forMerchant: parseInt(req.body.allowMerchant),
            panduanBayar: req.body.stepBayar,
            feeType: req.body.feeType,
            feeNominal: req.body.feeNominal
        });
        utilSim.httpSimPost('/systems/metodeBayar', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/metodeBayar/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('pg', 'invalid pg').not().isEmpty().withMessage('Payment Gateway harus diisi')
    req.check('bankId', 'invalid path').not().isEmpty().withMessage('ID Bank harus diisi');
    req.check('bankName', 'invalid icon').not().isEmpty().withMessage('Nama Bank Harus diisi');
    req.check('logo', 'invalid icon').not().isEmpty().withMessage('URL Logo harus diisi');
    req.check('deskripsi', 'invalid icon').not().isEmpty().withMessage('deskripsi harus diisi');
    req.check('stepBayar', 'invalid stepBayar').not().isEmpty().withMessage('Langlah pembayaran harus diisi');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({            
            id : req.body.id,
            idPg: req.body.pg,
            idBank: req.body.bankId,
            namaBank: req.body.bankName,
            deskripsi: req.body.deskripsi,
            logo: req.body.logo,
            forRk: parseInt(req.body.allowRk),
            forDineIn: parseInt(req.body.allowDineIn),
            forMerchant: parseInt(req.body.allowMerchant),
            panduanBayar: req.body.stepBayar,
            feeType: req.body.feeType,
            feeNominal: req.body.feeNominal
        });

        utilSim.httpSimPut('/systems/metodeBayar/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;