const express = require('express');
const utilSim = require('../../core/utilSim');
const e = require('express');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/transaksiMerchant', {data:null, title: "Transaksi Merchant", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    let lunas = '&lunas=' + req.query.lunas;
    let via = req.query.via;
    let tanggal = req.query.tanggal;
    let tanggalBeli = req.query.tanggalBeli;
    
    if(req.query.lunas == undefined){
        lunas = '';
    }
    if(via == undefined){
        via = null;
    }
    if(tanggal == undefined){
        tanggal = null;
    }
    if(tanggalBeli == undefined){
        tanggalBeli = null;
    }
    
    utilSim.httpSimGet('/systems/transaksiMerchant?tanggal=' + tanggal + lunas + '&via=' + via + '&tanggalBeli=' + tanggalBeli + '&page='+req.query.page 
    +'&limit=' + req.query.limit + '&statusProses=' + req.query.statusProses, req, res, function(result){
        if(result.rc == "00"){
            let jumlahPage = Math.round(result.total / req.query.limit)
            res.json({success :true, data: result.data, jumlahData: jumlahPage})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/' + req.query.id, req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

router.get('/cekResi',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/ongkir/wayBill?waybill='+req.query.kurirResi+'&courier=' + req.query.kurirKode, req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.object.data})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

router.get('/detailTransaksi',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksiMerchant/' + req.query.id, req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

router.post('/sukses', utilSim.checkUserSession, (req, res, next) => {
    req.check('idTransaksi', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    
    var errors = req.validationErrors();
    
    let userInput = JSON.stringify({
        no_reff: req.body.idTransaksi,
    });
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimPost('/systems/transaksi/sukses', userInput, req, function(result){
            if(result == "ACCEPTED"){
                res.json({errors: errors, success :true, message: result.rd, data: result})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    }
});

router.post('/updateStatus', utilSim.checkUserSession, (req, res, next) => {
    req.check('orderId', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    req.check('status', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    
    let userInput = JSON.stringify({
        orderId: req.body.orderId,
        status: req.body.status
    });
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimPost('/systems/transaksi/updateStatusMerchant', userInput, req, function(result){
            if(result == "ACCEPTED"){
                res.json({errors: errors, success :true, message: result.rd, data: result})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    }
});

router.post('/updateResi', utilSim.checkUserSession, (req, res, next) => {
    req.check('idTransaksi', 'invalid data').not().isEmpty().withMessage('Invalid ID Transaksi')
    req.check('noResi', 'invalid Nomor Resi').not().isEmpty().withMessage('Nomor Resi Harus Diisi')
    
    var errors = req.validationErrors();
    
    let userInput = JSON.stringify({
        id: req.body.idTransaksi,
        kurirNoResi: req.body.noResi,
    });
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimPut('/systems/transaksi/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd, data: result})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    }
});

module.exports = router;