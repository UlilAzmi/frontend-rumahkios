const express = require('express');
const XLXS = require('xlsx');
var xlsxtojson = require("xlsx-to-json-lc");
var parser = require('simple-excel-to-json')
const utilSim = require('../core/utilSim');
const multer = require('multer');
const router = express.Router();

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./datas");
    },
    filename: function(req, file, callback) {
        callback(null, "data_import.xlsx");
    },
});

const upload = multer({
    storage: Storage,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(xlsx)$/)){
            file.mimetype = '.xlsx'
        // }else if(file.originalname.match(/\.(xlsx)$/)){
        //     file.mimetype = '.xlsx'
        }else{
            return cb(new Error('only .xlsx file type'))
        }
        
        cb(undefined, true)
    },
}).single("file");

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/users?role=58', req, res, function(result){
        res.render('siswaKelas', {data:result.data, title: "Siswa Kelas", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user)});
    })
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/users/siswa?kelas='+req.query.kelas+'&waliKelas=&tahun='+req.query.tahun+'&tingkat='+req.query.tingkat+'&jurusan='+req.query.jurusan, req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/siswaKelas/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/upload', utilSim.checkUserSession, (req, res, next) => {
    upload(req, res, function(err) {
        if (err) {
            return res.send({ success :false, message: err.message});
        }

        /** Multer gives us file info in req.file object */
        if(!req.file){
            res.json({error_code:1,err_desc:"No file passed"});
            return;
        }
        /** Check the extension of the incoming file and
         *  use the appropriate module
         */
        if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
            exceltojson = xlsxtojson;
        }

        try {
            var doc = parser.parseXls2Json(req.file.path, { isNested: true });
            console.log(doc[0])
            res.json({success :true, error_code: 0, err_desc:null, data: doc});
            /*exceltojson({
                input: req.file.path,
                output: null, //since we don't need output.json
                lowerCaseHeaders:true
            }, function(err,result){
                if(err) {
                    return res.json({error_code:1,err_desc:err, data: null});
                }
                console.log(result)
                res.json({success :true, error_code:0,err_desc:null, data: result});
            });*/
        } catch (e){
            res.json({error_code:1,err_desc:"Corupted excel file"});
        }

        // const workbook = XLXS.readFile('./datas/data_import.xlsx');
        // const sheet = workbook.SheetNames;
        // console.log()
        // res.json({success :true, data: "Berhasil diupload"})
    });
}, (error, req, res, next) => {
    res.status(200).send({error: error.message})
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('wali', 'invalid user wali').not().isEmpty().withMessage('User wali required')
    req.check('kelas', 'invalid kelas').not().isEmpty().withMessage('kelas is required');
    req.check('tahun', 'invalid tahun').not().isEmpty().withMessage('Tahun ajaran is required');
x
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            wali : {id : parseInt(req.body.wali) } ,
            kelas : {id : parseInt(req.body.kelas) } ,
            tahun : {id : parseInt(req.body.tahun) },
            keterangan: req.body.keterangan,  
        });
        utilSim.httpSimPost('/systems/siswaKelas', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/siswaKelas/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('wali', 'invalid user wali').not().isEmpty().withMessage('User wali required')
    req.check('kelas', 'invalid kelas').not().isEmpty().withMessage('kelas is required');
    req.check('tahun', 'invalid tahun').not().isEmpty().withMessage('Tahun ajaran is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            wali : {id : parseInt(req.body.wali) } ,
            kelas : {id : parseInt(req.body.kelas) } ,
            tahun : {id : parseInt(req.body.tahun) },
            keterangan: req.body.keterangan,  
        });

        utilSim.httpSimPut('/systems/siswaKelas/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;