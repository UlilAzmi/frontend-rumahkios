const express = require('express');
// const Home = require('../core/c_home');
const utilSim = require('../../core/utilSim');
const router = express.Router();
// const home = new Home();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    
    var obj =JSON.parse(req.session.menu);
    if(obj.role.id == 1){
        res.render('admin/home', {title: "Home", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
    }else{
        res.render('admin/myHome', {title: "Home", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
    }
});

router.get('/payToday',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/payToday', req, res, function(result){
        res.json({success :true, data: result})
    })
});

router.get('/income',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/income', req, res, function(result){
        res.json({success :true, data: result})
    })
});

router.get('/blmBayar',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/blmBayar', req, res, function(result){
        res.json({success :true, data: result})
    })
});

router.get('/sudahBayar',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/sudahBayar', req, res, function(result){
        res.json({success :true, data: result})
    })
});

module.exports = router;