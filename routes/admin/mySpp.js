const express = require('express');
const utilSim = require('../core/utilSim');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/mySpp', req, res, function(result){
        res.render('mySpp', {data:result.data, title: "My SPP", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user)});
    })
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/mySpp', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/mySpp/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');
    req.check('tingkat', 'invalid tingkat').not().isEmpty().withMessage('tingkat is required');
    req.check('jurusan', 'invalid jurusan').not().isEmpty().withMessage('jurusan is required');
    req.check('bulanAwal', 'invalid mulai bulan').not().isEmpty().withMessage('mulai bulan is required');
    req.check('bulanAkhir', 'invalid end bulan').not().isEmpty().withMessage('end bulan is required');
    
    if(req.body.bulanAkhir != '' && req.body.bulanAwal != '' && req.body.bulanAwal !== req.body.bulanAkhir){
        req.check('bulanAwal', 'invalid').isBefore(req.body.bulanAkhir).withMessage('Mulai bulan harus < End bulan');
    }

    var errors = req.validationErrors();    
    if(errors){
        res.json({errors: errors})
    }else{
        var arrJurusan = Array();
        var arrKelas = Array();

        req.body.jurusan.forEach(element => arrJurusan.push(parseInt(element)));
        req.body.kelas.forEach(element => arrKelas.push(parseInt(element)));

        let userInput = JSON.stringify({
            namaTagihan: req.body.nama,
            keterangan: req.body.keterangan,
            jurusanId : arrJurusan ,
            tingkatId : parseInt(req.body.tingkat) ,
            kelasId : arrKelas , 
            productId : parseInt(req.body.idPembayaran) ,
            mulaiBulan : req.body.bulanAwal ,
            sampaiBulan : req.body.bulanAkhir 
        });
        console.log(userInput)
        utilSim.httpSimPost('/systems/mySpp', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/mySpp/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');
    req.check('tingkat', 'invalid tingkat').not().isEmpty().withMessage('tingkat is tingkat');
    req.check('jurusan', 'invalid jurusan').not().isEmpty().withMessage('jurusan is jurusan');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            nama: req.body.nama,
            keterangan: req.body.keterangan,
            jurusan : {id : parseInt(req.body.jurusan) } ,
            tingkat : {id : parseInt(req.body.tingkat) }  
        });

        utilSim.httpSimPut('/systems/mySpp/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;