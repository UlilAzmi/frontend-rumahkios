const express = require('express');
const utilSim = require('../../core/utilSim');
let fs = require('fs');
const router = express.Router();


router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/connectWa', {title: "Koneksi Whatsapp", 
    menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.post('/start', utilSim.checkUserSession, (req, res, next) => {
    let { exec } = require('child_process');
    switch (req.body.action) {
        case 'relogin':
            exec('./public/spv.sh stop', (error, stdout, stderr) => {
                let result = ["Service Whatsapp Terhenti"];
                let is_success = true;
                try {
                    fs.unlinkSync("./public/tmp/qrcode.png");
                    result.push("QR Code Terhapus");
                    console.log('QR Code Terhapus')
                    is_success = true;
                } catch (err) {
                    result.push("Gagal menghapus QR Code");
                    console.log('Gagal menghapus QR Code ' + err)
                    is_success = false;
                }
                try {
                    fs.unlinkSync("./public/tmp/session.gob");
                    console.log("GOB File Terhapus")
                    result.push("GOB File Terhapus");
                    is_success = true;
                } catch (err) {
                    result.push("Gagal menghapus GOB File");
                    result.push("Gagal menghapus GOB File " + err);
                    is_success = false;
                }

                console.log("Is Success " + is_success);
                if (true) {
                    exec('./public/spv.sh start &');
                    result.push("Service Whatsapp Mulai");
                    console.log("Service Whatsapp Mulai");
                    setTimeout(() =>  res.json({success :true, data: result}), 3000);
                } else {
                    res.status(500);
                    res.send(result.join("\n"));
                }
            });
            break;
        case 'start':
            exec('./public/spv.sh start &');
            console.log('Service Whatsapp Mulai');
            let result = ["Service Whatsapp Mulai"];
            setTimeout(() =>  res.json({success :true, data: result}), 3000);
            break;
        case 'stop':
            exec('./public/spv.sh stop &');
            console.log('Service Whatsapp Terhenti');
            let result1 = ["Service Whatsapp henti"];
            setTimeout(() =>  res.json({success :true, data: result1}), 3000);
            break;
        case 'status':
            exec('./public/spv.sh show &');
            console.log('Service Whatsapp Status');
            break;
        default:
            res.status(400);
            res.send("Unregistered Service");
            break;
    }
});


router.post('/test', utilSim.checkUserSession, (req, res, next) => {
    req.check('noTelp', 'invalid no Telp').not().isEmpty().withMessage('No Wa Harus diisi')
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            noWa: req.body.noTelp,
        });
        
        utilSim.httpSimPost('/systems/notifWa/test', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;