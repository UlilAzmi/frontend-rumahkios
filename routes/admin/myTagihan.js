const express = require('express');
const utilSim = require('../../core/utilSim');
const e = require('express');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/myTagihan', {data:null, title: "Tagihan Saya", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    // let lunas = '&lunas=' + req.query.lunas;
    // let via = req.query.via;
    // let tanggal = req.query.tanggal;
    // let tanggalBeli = req.query.tanggalBeli;
    
    // if(req.query.lunas == undefined){
    //     lunas = '';
    // }
    // if(via == undefined){
    //     via = null;
    // }
    // if(tanggal == undefined){
    //     tanggal = null;
    // }
    // if(tanggalBeli == undefined){
    //     tanggalBeli = null;
    // }

    utilSim.httpSimGet('/systems/tagihan/myTagihan', req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

router.get('/myTransaksi1',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/transaksi/myTransaksi', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }else{
        utilSim.httpSimGet('/systems/transaksi/myTransaksi', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }
});

router.get('/detail',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/tagihan/detail/' + req.query.id, req, res, function(result){
        if(result.object.data != null){
            res.json({success :true, data: result.object.data})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

router.get('/detailTagihan',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/tagihan/' + req.query.id, req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, message: result.rd})
        }
    })
});

module.exports = router;