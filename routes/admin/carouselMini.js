const express = require('express');
const utilSim = require('../../core/utilSim');
const multer = require('multer');
const fs = require('fs');

const router = express.Router();

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/datas/carouselMini");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});

const upload = multer({
    storage: Storage,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }
	else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('only .jpeg file type'))
        }
        
        cb(undefined, true)
    },
}).single("file");

router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/carouselMini', { title: "Setting Mini Carousel", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/carouselMini', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/carouselMini/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    // req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    // req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');
    
    // var errors = req.validationErrors();
    // if(errors){
    //     res.json({errors: errors})
    // }else{
    //     let userInput = JSON.stringify({
    //         nama: req.body.nama,
    //         keterangan: req.body.keterangan,
    //     });
        
    //     utilSim.httpSimPost('/systems/carouselMini', userInput, req, function(result){
    //         console.log(result)
    //         if(result.rc == "00"){
    //             res.json({errors: errors, success :true, message: result.rd})
    //         }else{
    //             res.json({errors: errors, success :false, message: result.rd})
    //         }
    //     });
    // } 

    upload(req, res, function(err) {
        //var b64 = new Buffer(fs.readFileSync(req.file.path)).toString("base64");
        if (err) {
            console.log('Error')
            return res.send({ success :false, message: err.message});
        }

        let userInput = JSON.stringify({
            nama: req.body.nama,
            status: req.body.status,
            urutan: req.body.urutan,
            link: req.body.link,
            src: req.file.path,
            // src: 'data:image/jpeg;base64,' + b64,
        });
        
        utilSim.httpSimPost('/systems/carouselMini', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    })
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    console.log(req.body.id)
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/carouselMini/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                fs.unlink(req.body.path);
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    upload(req, res, function(err) {
        var b64 = null;
        let userInput;
        if(req.file !== undefined){
            //b64 = 'data:image/jpeg;base64,' + new Buffer(fs.readFileSync(req.file.path)).toString("base64");
            fs.unlink(req.body.pathLama);
            userInput = JSON.stringify({
                id: parseInt(req.body.id),
                nama: req.body.nama,
                status: req.body.status,
                urutan: req.body.urutan,
                link: req.body.link,
                src: req.file.path,
            });
        }else{
            userInput = JSON.stringify({
                id: parseInt(req.body.id),
                nama: req.body.nama,
                status: req.body.status,
                urutan: req.body.urutan,
                link: req.body.link
            });
        }
        
        utilSim.httpSimPut('/systems/carouselMini', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
        
    })
});

module.exports = router;
