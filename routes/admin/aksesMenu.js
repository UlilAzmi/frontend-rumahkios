const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/aksesMenu', {title: "Akses Menu", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout:'admin'});
}); 

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/akses', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/filter',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/akses?role='+req.query.roleId, req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/akses/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('path', 'invalid path').not().isEmpty().withMessage('path is required');
    req.check('icon', 'invalid icon').not().isEmpty().withMessage('icon is required');
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({            
            role: {id : parseInt(req.body.role)},
            parentId: parseInt(req.body.parentId),
            nama: req.body.nama,
            path: req.body.path,
            icon: req.body.icon,
            create: req.body.create,
            read: req.body.read,
            update: req.body.update,
            delete: req.body.delete
        });
        utilSim.httpSimPost('/systems/akses', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/akses/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('path', 'invalid path').not().isEmpty().withMessage('path is required');
    req.check('icon', 'invalid icon').not().isEmpty().withMessage('icon is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({            
            id : req.body.id,
            role: {id : parseInt(req.body.role)},
            parentId: parseInt(req.body.parentId),
            nama: req.body.nama,
            path: req.body.path,
            icon: req.body.icon,
            create: req.body.create,
            read: req.body.read,
            update: req.body.update,
            delete: req.body.delete
        });

        utilSim.httpSimPut('/systems/akses/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;