const express = require('express');
const utilSim = require('../../core/utilSim');
const multer = require('multer');
const fs = require('fs');

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/datas/carousel");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});

const upload = multer({
    storage: Storage,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('only .jpeg file type'))
        }
        
        cb(undefined, true)
    },
}).single("file");

const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/carousel', { title: "Setting Carousel", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/carousel', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/carousel/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    upload(req, res, function(err) {
        // req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
        // req.check('active', 'invalid status').not().isEmpty().withMessage('Status is required')
        // req.check('urutan', 'invalid urutan').not().isEmpty().withMessage('Urutan is required')
        // req.check('link', 'invalid link').not().isEmpty().withMessage('Link is required')
        // req.check('src', 'invalid src').not().isEmpty().withMessage('Gambar is required')
        console.log('Masuk 1')
        if (err) {
            console.log('Error')
            return res.send({ success :false, message: err.message});
        }

       // var b64 = new Buffer(fs.readFileSync(req.file.path)).toString("base64");
        //fs.unlink(req.file.path);
        
        
        let userInput = JSON.stringify({
            nama: req.body.nama,
            active: req.body.active,
            urutan: req.body.urutan,
            link: req.body.link,
            src: req.file.path,
            // src: 'data:image/jpeg;base64,' + b64,
        });
        
        utilSim.httpSimPost('/systems/carousel', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
        
    })
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    console.log(req.body.id)
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/carousel/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                fs.unlink(req.body.path, function(err, result) {
                    if(err) console.log('error', err);
                });
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    // console.log('masuk')
    // req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    // req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');

    // var errors = req.validationErrors();
    // if(errors){
    //     res.json({errors: errors})
    // }else{
    //     let userInput = JSON.stringify({
    //         id : req.body.id,
    //         nama: req.body.nama,
    //         keterangan: req.body.keterangan
    //     });

    //     utilSim.httpSimPut('/systems/carousel/', userInput, req, function(result){
    //         if(result.rc == "00"){
    //             res.json({errors: errors, success :true, message: result.rd})
    //         }else{
    //             res.json({errors: errors, success :false, message: result.rd})
    //         }
    //     });
    // }
    
    upload(req, res, function(err) {
        var b64 = null;
        let userInput ;
        if(req.file !== undefined){
            if (err) {
                console.log('Error')
                return res.send({ success :false, message: err.message});
            }
            userInput = JSON.stringify({
                id: parseInt(req.body.id),
                nama: req.body.nama,
                active: req.body.active,
                urutan: req.body.urutan,
                link: req.body.link,
                src: req.file.path
            });
            //b64 = 'data:image/jpeg;base64,' + new Buffer(fs.readFileSync(req.file.path)).toString("base64");
            fs.unlink(req.body.pathLama, function(err, result) {
                if(err) console.log('error', err);
            });
        }else{
            userInput = JSON.stringify({
                id: parseInt(req.body.id),
                nama: req.body.nama,
                active: req.body.active,
                urutan: req.body.urutan,
                link: req.body.link
            });
        }

        utilSim.httpSimPut('/systems/carousel', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
        
    })
});

module.exports = router;