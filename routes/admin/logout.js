const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();
const storage = require('node-sessionstorage')

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    console.log('Signout Process...')
    cookie = req.cookies;
    for (var prop in cookie) {
        if (!cookie.hasOwnProperty(prop)) {
            continue;
        }    
        res.cookie(prop, '', {expires: new Date(0)});
    }
    req.session.destroy();
    storage.removeItem('menu');
    console.log('Success Signout')
    res.render('login', {
        title: "Login", 
        layout: false,
        success: false, 
        message: 'You Are SignOut'
        })
});

module.exports = router;