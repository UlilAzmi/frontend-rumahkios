const express = require('express');
const utilSim = require('../../core/utilSim');
const e = require('express');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/pelimpahan', {data:null, title: "Pelimpahan", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    let sellerId = req.query.sellerId;

    let sDateInq = req.query.sDateInq;
    let eDateInq = req.query.eDateInq;
    
    let sDatePay = req.query.sDatePay;
    let eDatePay = req.query.eDatePay;

    let lunas = '&lunas=' + req.query.lunas;
    let via = req.query.via;
    let terlimpahkan =  req.query.terlimpahkan;
    
    if(sDateInq == undefined){
        sDateInq = null;
    }
    if(eDateInq == undefined){
        eDateInq = null;
    }
    if(sDatePay == undefined){
        sDatePay = null;
    }
    if(eDatePay == undefined){
        eDatePay = null;
    }

    if(req.query.lunas == undefined){
        lunas = '';
    }
    if(via == undefined){
        via = null;
    }
    if(terlimpahkan == undefined){
        terlimpahkan = null;
    }

    utilSim.httpSimGet('/systems/transaksi/listPelimpahan?sDateInq='+sDateInq+'&eDateInq='+eDateInq+'&sDatePay='+sDatePay+'&eDatePay='+eDatePay
    +'&sellerId='+sellerId+'&via='+via+ lunas+'&terlimpahkan=' + terlimpahkan + '&page='+req.query.page +'&limit=' + req.query.limit, req, res, function(result){
        if(result.rc == "00"){
            let jumlahPage = Math.round(result.total / req.query.limit)
            res.json({errors: false, data: result.data, success :true, message: result.rd, jumlahData: jumlahPage})
        }else{
            let jumlahPage = Math.round(result.total / req.query.limit)
            res.json({errors: true, success :false, message: result.rd})
        }
        
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/pelimpahan/' + req.query.id, req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, data: result.rd})
        }
    })
});

router.get('/apply',utilSim.checkUserSession, (req, res, next) => {
    let listId = req.query.listId;

    if(listId == undefined){
        listId = null;
    }

    utilSim.httpSimGet('/systems/transaksi/checkTransaksi?listId='+listId, req, res, function(result){
        if(result.rc == "00"){
            res.json({errors: false, data: result.object.data, success :true, message: result.rd})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
        
    })
});

router.post('/doPelimpahan', utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimPost('/systems/transaksi/doPelimpahan', req.body.selectedId.toString(), req, function(result){
        if(result.rc == "00"){
            res.json({errors: false, success :true, message: result.rd})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
    });
});

module.exports = router;