const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/productMember', req, res, function(result){
        // res.json({success :true, data: result.data[0]})
        
        for(i = 0 ; i < result.data.length ; i++){
            result.data[i].strHarga = new Number(result.data[i].harga).toLocaleString("id-ID");
            console.log(result.data[i])
        }

        res.render('admin/beliProduk', { title: "Beli Produk", menu: JSON.parse(req.session.menu), 
        user: JSON.parse(req.session.user), layout: 'admin', data: result});
    })
});

module.exports = router;