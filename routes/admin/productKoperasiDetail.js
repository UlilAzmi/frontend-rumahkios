
const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/productKoperasiDetail', {title: "Product Koperasi Detail", 
    menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/productKoperasiDetail', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/productKoperasiDetail/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Nama tidak boleh kosong')
    req.check('harga', 'invalid keterangan').not().isEmpty().withMessage('harga tidak boleh kosong');
    req.check('biller', 'invalid biller').not().isEmpty().withMessage('feeBiller tidak boleh kosong');
    

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            nama: req.body.nama,
            harga: req.body.harga,
            feeBiller: req.body.biller,
            keterangan: req.body.keterangan
        });
        
        utilSim.httpSimPost('/systems/productKoperasiDetail', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/productKoperasiDetail/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Nama tidak boleh kosong')
    req.check('harga', 'invalid keterangan').not().isEmpty().withMessage('harga tidak boleh kosong');
    req.check('biller', 'invalid biller').not().isEmpty().withMessage('feeBiller tidak boleh kosong');
    
    var errors = req.validationErrors();
    if(errors){
        console.log('error')
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id: req.body.id,
            nama: req.body.nama,
            harga: req.body.harga,
            feeBiller: req.body.biller,
            keterangan: req.body.keterangan
        });

        utilSim.httpSimPut('/systems/productKoperasiDetail/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

module.exports = router;