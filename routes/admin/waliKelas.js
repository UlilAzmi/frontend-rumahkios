const express = require('express');
const utilSim = require('../core/utilSim');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/waliKelas', req, res, function(result){
        res.render('waliKelas', {data:result.data, title: "Wali Kelas", menu: JSON.parse(utilSim.getMenu(req, res, next)), user: JSON.parse(req.session.user)});
    })
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/waliKelas?waliId='+req.query.wali+'&kelasId='+req.query.kelas+'&tahunId='+req.query.tahun, req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/waliKelas/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.get('/filter',utilSim.checkUserSession, (req, res, next) => {
    var qryJurusan = "";
    if('' != req.query.jurusanId && null != req.query.jurusanId){
        qryJurusan = "&jurusanId=" + req.query.jurusanId;
    }
    utilSim.httpSimGet('/systems/waliKelas?' + req.query.id + qryJurusan, req, res, function(result){
        var jurusan = Array();
        var tmpJurusan = Array();
        var kelas = Array();
        var tmpKelas = Array();
        result.data.forEach(value => { 
            if(null != value.kelas.jurusan && !tmpJurusan.includes(value.kelas.jurusan.id)){
                var valueJurusan = {};
                valueJurusan["id"] = value.kelas.jurusan.id;
                valueJurusan["nama"] = value.kelas.jurusan.nama;
                tmpJurusan.push(value.kelas.jurusan.id);
                jurusan.push(valueJurusan);    
            }

            if(null != value.kelas && !tmpKelas.includes(value.kelas.id)){
            var valueKelas = {};
                valueKelas["id"] = value.kelas.id;
                valueKelas["nama"] = value.kelas.nama;
                tmpKelas.push(value.id);
                kelas.push(valueKelas);    
            }
        });
        res.json({success :true, data: result.data, jurusan: jurusan, kelas: kelas})
    })
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('wali', 'invalid user wali').not().isEmpty().withMessage('User wali required')
    req.check('kelas', 'invalid kelas').not().isEmpty().withMessage('kelas is required');
    req.check('tahun', 'invalid tahun').not().isEmpty().withMessage('Tahun ajaran is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            wali : {id : parseInt(req.body.wali) } ,
            kelas : {id : parseInt(req.body.kelas) } ,
            tahun : {id : parseInt(req.body.tahun) },
            keterangan: req.body.keterangan,  
        });
        utilSim.httpSimPost('/systems/waliKelas', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/waliKelas/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('wali', 'invalid user wali').not().isEmpty().withMessage('User wali required')
    req.check('kelas', 'invalid kelas').not().isEmpty().withMessage('kelas is required');
    req.check('tahun', 'invalid tahun').not().isEmpty().withMessage('Tahun ajaran is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            wali : {id : parseInt(req.body.wali) } ,
            kelas : {id : parseInt(req.body.kelas) } ,
            tahun : {id : parseInt(req.body.tahun) },
            keterangan: req.body.keterangan,  
        });

        utilSim.httpSimPut('/systems/waliKelas/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;