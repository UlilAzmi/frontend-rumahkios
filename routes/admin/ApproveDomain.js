const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/approveDomain', { title: "Approve Domain", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/approveDomain', req, res, function(result){
        console.log(result.data)
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/approveDomain/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('pesanKeMerchant', 'invalid Primary Key 2').not().isEmpty().withMessage('Pesan ke merchant harus diisi')
    
    if(req.body.statusApprove === '1'){
        req.check('domain', 'invalid Domain').not().isEmpty().withMessage('Domain Approve Harus diisi')    
    }

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            domain: req.body.domain,
            statusDomain: req.body.statusApprove,
            pesanKeMerchant: req.body.pesanKeMerchant
        });

        utilSim.httpSimPut('/systems/approveDomain/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;