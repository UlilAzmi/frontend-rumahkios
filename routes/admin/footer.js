const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/footer', { title: "Setting Footer", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/footer', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/footer/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});


router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('ig', 'invalid nama').not().isEmpty().withMessage('Name is required')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            about: req.body.about,
            facebook: req.body.fb,
            twitter: req.body.twitter,
            instagram: req.body.ig,
            googlePlus: req.body.gp,
            alamat: req.body.alamat
        });
        utilSim.httpSimPut('/systems/footer', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;