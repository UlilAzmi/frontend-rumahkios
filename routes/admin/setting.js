const express = require('express');
const Setting = require('../core/c_setting');
const router = express.Router();

const setting = new Setting();

router.get('/',checkUserSession, (req, res, next) => {
    setting.findAll('Y', function(result){
        if(result){
            res.render('setting', {data: result, title: "Setting", menu: req.session.menu, profile:JSON.parse(req.session.user)});
        }
    });
});

router.get('/edit',checkUserSession, (req, res, next) => {
    setting.find(req.query.id, function(result){
        if(result){
            res.json({success :true, data: result})
        }
    });
});

router.post('/update', checkUserSession, (req, res, next) => {
    for(var item in req.body) {
        let userInput = {
            value: req.body[item].nilai,
            last_update: new Date(),
            last_update_who: req.session.user.id,
            idPosition: req.body[item].id,
            idMenu:req.body[item].idMenu,
        };
        setting.update(userInput, function(lastId){
            if(lastId){
                //res.json({errors: errors, success :true, message: lastId})
            }else{
                res.json({errors: errors, success :false, message: "Error creating a new user ..."})
            }
        });
    }
    res.json({errors: false, success :true, message: "Sukses update!"})

});

function checkUserSession( req, res, next )
{
    if( req.session.user )
    {
        next();
    }
    else
    {
        res.redirect('/login');
    }
}

module.exports = router;