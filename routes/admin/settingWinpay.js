const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/settingWinpay', { title: "Setting Winpay", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/settingWinpay', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/toolbar',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && 'devel' == env){
        utilSim.httpSimGetDevel('/systems/settingWinpay/toolbar', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }else{
        utilSim.httpSimGet('/systems/settingWinpay/toolbar', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/settingWinpay/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('pk1', 'invalid Primary Key 1').not().isEmpty().withMessage('Primary Key 1 is required')
    req.check('pk2', 'invalid Primary Key 2').not().isEmpty().withMessage('Primary Key 2 is required')
    req.check('mk', 'invalid Merchant Key').not().isEmpty().withMessage('Merchant Key is required')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            primaryKey1: req.body.pk1,
            primaryKey2: req.body.pk2,
            merchantKey: req.body.mk
        });

        utilSim.httpSimPut('/systems/settingWinpay/', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;