const express = require('express');
const utilSim = require('../../core/utilSim');
const multer = require('multer');
var moment = require('moment');
const fs = require('fs');
const { min } = require('moment');
const { ADDRGETNETWORKPARAMS } = require('dns');
const router = express.Router();

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "../toko/public/datas/productMerchant");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});

const upload = multer({
    storage: Storage,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('hanya file foto type jpeg, jpg dan png '))
        }
        
        cb(undefined, true)
    },
}).single("file");

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/myProduct', {title: "Produk Saya", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/myProduct', req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, errors: false, data: result.data})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/myProduct/' + req.query.id, req, res, function(result){
        console.log(result.data[0])
        res.json({success :true, data: result.data[0]})
    })
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    upload(req, res, function(err) {
        req.checkBody('nama', 'invalid nama').not().isEmpty().withMessage('nama harus diisi')
        req.checkBody('group', 'invalid group').not().isEmpty().withMessage('Group Produk harus diisi')
        req.checkBody('hargaBeli', 'invalid harga Beli').not().isEmpty().withMessage('harga beli harus diisi')
        req.checkBody('hargaJual', 'invalid harga jual').not().isEmpty().withMessage('Harga jual harus diisi')
        req.checkBody('hargaCoret', 'invalid harga coret').not().isEmpty().withMessage('harga coret harus diisi')
        req.checkBody('berat', 'invalid Berat').isNumeric({min: 1}).withMessage('Berat Produk Harus diisi')
        req.checkBody('jumlahStok', 'invalid Jumlah Stok').isNumeric({min: 1}).withMessage('Jumlah Stok Harus diisi')
        
        var errors = req.validationErrors();
        if(errors){
            res.json({errors: errors})
        }else{
            if (err) {
                return res.send({ success :false, message: err.message});
            }

            let userInput = JSON.stringify({
                nama: req.body.nama,
                image: req.file.path,
                isActive: req.body.active,
                hargaCoret: req.body.hargaCoret,
                harga: req.body.hargaJual,
                hargaBeli: req.body.hargaBeli,
                keterangan: req.body.keterangan,
                weight: req.body.berat,
                promo: req.body.promo,
                stok : req.body.jumlahStok,
                isOngkir: req.body.ongkir,
                groupProductId: req.body.group
            });
            
            utilSim.httpSimPost('/systems/myProduct', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: false, success :true, message: result.rd})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });   
        }
    })
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/myProduct/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                fs.unlink(req.body.path, function(err, result) {
                    if(err) console.log('error', err);
                });
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    upload(req, res, function(err) {
        req.checkBody('nama', 'invalid nama').not().isEmpty().withMessage('nama harus diisi')
        req.checkBody('group', 'invalid group').not().isEmpty().withMessage('Group Produk harus diisi')
        req.checkBody('hargaBeli', 'invalid harga Beli').not().isEmpty().withMessage('harga beli harus diisi')
        req.checkBody('hargaJual', 'invalid harga jual').not().isEmpty().withMessage('Harga jual harus diisi')
        req.checkBody('hargaCoret', 'invalid harga coret').not().isEmpty().withMessage('harga coret harus diisi')
        req.checkBody('berat', 'invalid Berat').isNumeric({min: 1}).withMessage('Berat Produk Harus diisi')
        req.checkBody('jumlahStok', 'invalid Jumlah Stok').isNumeric({min: 1}).withMessage('Jumlah Stok Harus diisi')

        var errors = req.validationErrors();
        if(errors){
            res.json({errors: errors})
        }else{
            let userInput;
            if (err) {
                return res.send({ success :false, message: err.message});
            }
            
            if(req.file !== undefined){
                userInput = JSON.stringify({
                    id: parseInt(req.body.id),
                    nama: req.body.nama,
                    image: req.file.path,
                    isActive: req.body.active,
                    hargaCoret: req.body.hargaCoret,
                    harga: req.body.hargaJual,
                    hargaBeli: req.body.hargaBeli,
                    keterangan: req.body.keterangan,
                    weight: req.body.berat,
                    promo: req.body.promo,
                    stok : req.body.jumlahStok,
                    isOngkir: req.body.ongkir,
                    groupProductId: req.body.group
                });
                fs.unlink(req.body.pathLama, function(err, result) {
                    if(err) console.log('error', err);
                });
            }else{
                userInput = JSON.stringify({
                    id: parseInt(req.body.id),
                    nama: req.body.nama,
                    isActive: req.body.active,
                    hargaCoret: req.body.hargaCoret,
                    harga: req.body.hargaJual,
                    hargaBeli: req.body.hargaBeli,
                    keterangan: req.body.keterangan,
                    weight: req.body.berat,
                    promo: req.body.promo,
                    stok : req.body.jumlahStok,
                    isOngkir: req.body.ongkir,
                    groupProductId: req.body.group
                });
            }
            
            utilSim.httpSimPut('/systems/myProduct/', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: false, success :true, message: result.rd})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });
        }            
    })
});

module.exports = router;
