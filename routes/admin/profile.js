const express = require('express');
const fs = require('fs');
const utilSim = require('../../core/utilSim');
const router = express.Router();
const multer = require('multer');
var StorageKtp = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/datas/ktp");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});
const uploadKtp = multer({
    storage: StorageKtp,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('hanya file foto type jpeg, jpg dan png '))
        }
        
        cb(undefined, true)
    },
}).single("file");

var StorageLogo = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "../toko/public/datas/logos");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});
const uploadLogo = multer({
    storage: StorageLogo,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('hanya file foto type jpeg, jpg dan png '))
        }
        
        cb(undefined, true)
    },
}).single("file");

var StorageBanner = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "../toko/public/datas/banners");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});
const uploadBanner = multer({
    storage: StorageBanner,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('hanya file foto type jpeg, jpg dan png '))
        }
        
        cb(undefined, true)
    },
}).single("file");

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/myProfile', req, res, function(result){
        let gender = '';
        console.log(result.data[0])
        if(result.data[0].gender == 'L'){
            gender = "Laki-laki"
        }else{
            gender = "Perempuan"
        }
        var member = "Member - ";
        if(result.data[0].roleId == 1){
            member = "ADMIN";
        }else if(result.data[0].roleId == 2){
            member = "Member Basic";
        }else if(result.data[0].roleId == 3){
            member = "Member Premium";
        }else if(result.data[0].roleId == 4){
            member = "Member Smart";
        }
        
        let domainClass = "";
        let statusDomain = "Belum Diverifikasi";
        if(null != result.data[0].statusDomain){
            if('1' === result.data[0].statusDomain){
                statusDomain = "Terverifikasi";
                domainClass = "is-valid";
            }else{
                domainClass = "is-invalid";
            }
        }
        let bankStatus = "-";
        let bankClass = "";
        let pathLogo =  result.data[0].pathLogo.replace("../toko/public/datas/logos", "logos");
        let pathKtp = "";

        if(null != result.data[0].pathKtp){
            pathKtp = result.data[0].pathKtp.replace("public/", "");
        }
        
        let alamatLengkap = result.data[0].alamatToko + " Kecamatan " + result.data[0].kecamatanName + " Kab/Kota " + result.data[0].kotaName 
        + " " + result.data[0].provinsiName;

        if(result.data[0].userBankStatus === 1){
            bankStatus = "SUDAH Diverifikasi oleh admin";
            bankClass = "is-valid";
        }else{
            bankStatus = "BELUM Diverifikasi oleh admin"
            bankClass = "is-invalid";
        }
        res.render('admin/profile', {title: "my Profile", menu: JSON.parse(req.session.menu), 
        user: JSON.parse(req.session.user), layout: 'admin', nama: result.data[0].name, gift: result.data[0].gift, member: member,
        id: result.data[0].id, email: result.data[0].email, noTelp: result.data[0].noTelp, nik: result.data[0].nik,
        noTelpSiswa: result.data[0].noTelpSiswa, tempatLahir: result.data[0].tempatLahir + " " + result.data[0].tanggalLahir, 
        kelas: result.data[0].kelasName, jurusan: result.data[0].jurusanName, alamat: result.data[0].alamatKtp, alamatToko: result.data[0].alamatToko, kecamatan: result.data[0].kecamatanName,
        kota: result.data[0].kotaName, provinsi: result.data[0].provinsiName, gender: gender, instagram: result.data[0].instagram, facebook: result.data[0].facebook,
        noAbsen: result.data[0].noAbsen, alamatLengkap: alamatLengkap, domain1: result.data[0].domain1, domain2: result.data[0].domain2, 
        domain3: result.data[0].domain3, statusDomain : statusDomain, domainApprove : result.data[0].domain, bannerLogo: pathLogo, banners: result.data[0].listCarousel,
        bankId: result.data[0].userBankId, bankKode: result.data[0].userBankKode, bankName: result.data[0].userBankName, 
        bankAtasNama: result.data[0].userBankAtasNama, bankNoRek: result.data[0].userBankNoRek, bankCabang: result.data[0].userBankCabang, 
        bankStatus: bankStatus, bankLastUpdate: result.data[0].userBankLastUpdate, bankClass: bankClass, domainClass: domainClass , fotoKtp: pathKtp
        });
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/myProfile', req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/update/domain', utilSim.checkUserSession, (req, res, next) => {
    req.check('domain1', 'invalid Domain1').not().isEmpty().withMessage('Domain 1 harus diisi')
    req.check('domain2', 'invalid Domain2').not().isEmpty().withMessage('Domain 2 harus diisi');
    req.check('domain3', 'invalid Domain3').not().isEmpty().withMessage('Domain 3 harus diisi');
    
    req.check('eDomain1', 'invalid Domain1').not().isEmpty().withMessage('Domain 1 harus diisi')
    req.check('eDomain2', 'invalid Domain2').not().isEmpty().withMessage('Domain 2 harus diisi');
    req.check('eDomain3', 'invalid Domain3').not().isEmpty().withMessage('Domain 3 harus diisi');
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : parseInt(req.body.id),
            domain1: req.body.domain1 + req.body.eDomain1,
            domain2: req.body.domain2 + req.body.eDomain2,
            domain3: req.body.domain3 + req.body.eDomain3,
        });
        utilSim.httpSimPut('/systems/users/domain/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/update/alamat', utilSim.checkUserSession, (req, res, next) => {
    req.check('provinsiId', 'invalid Provinsi').not().isEmpty().withMessage('Provinsi harus diisi')
    req.check('kotaId', 'invalid Kota').not().isEmpty().withMessage('Kota harus diisi');
    req.check('kecamatanId', 'invalid Kecamatan').not().isEmpty().withMessage('Kecamatan harus diisi');
    req.check('alamat', 'invalid Detail Alamat').not().isEmpty().withMessage('Detail harus diisi');
        
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : parseInt(req.body.id),
            alamatToko: req.body.alamat,
            provinsiId: req.body.provinsiId,
            kotaId: req.body.kotaId,
            kecamatanId: req.body.kecamatanId,
        });
        utilSim.httpSimPut('/systems/users/alamat/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/updateLogo', utilSim.checkUserSession, (req, res, next) => {
    uploadLogo(req, res, function(err) {
        var errors = req.validationErrors();
        if(errors){
            res.json({errors: errors})
        }else{
            let userInput;
            if (err) {
                return res.send({ success :false, message: err.message});
            }
            
            if(req.file !== undefined){
                userInput = JSON.stringify({
                    id: parseInt(req.body.id),
                    pathLogo: req.file.path,
                });
                fs.unlink(req.body.pathLama, function(err, result) {
                    if(err) console.log('error', err);
                });
            }
            
            utilSim.httpSimPut('/systems/users/logo/', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: false, success :true, message: result.rd})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });
        }            
    })
});

router.post('/addBanner', utilSim.checkUserSession, (req, res, next) => {
    uploadBanner(req, res, function(err) {
        var errors = req.validationErrors();
        if(errors){
            res.json({errors: errors})
        }else{
            let userInput;
            if (err) {
                return res.send({ success :false, message: err.mesresponsesage});
            }
            
            if(req.file !== undefined){
                userInput = JSON.stringify({
                    urutan: req.body.urutan,
                    active: 1,
                    src: req.file.path,
                });
            }
            
            utilSim.httpSimPost('/systems/carousel', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: false, success :true, message: result.rd})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });
        }            
    })
});

router.get('/editBanner',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/carousel/'+req.query.id, req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
    })
});

router.post('/updateBanner', utilSim.checkUserSession, (req, res, next) => {
    uploadBanner(req, res, function(err) {
        var errors = req.validationErrors();
        if(errors){
            res.json({errors: errors})
        }else{
            let userInput;
            if (err) {
                return res.send({ success :false, message: err.message});
            }
            
            if(req.file !== undefined){
                userInput = JSON.stringify({
                    id: parseInt(req.body.id),
                    urutan: req.body.urutan,
                    src: req.file.path,
                });
                fs.unlink(req.body.pathLama, function(err, result) {
                    if(err) console.log('error', err);
                });
            }else{
                userInput = JSON.stringify({
                    id: parseInt(req.body.id),
                    urutan: req.body.urutan
                });
            }
            
            utilSim.httpSimPut('/systems/carousel', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: false, success :true, message: result.rd})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });
        }            
    })
});

router.post('/hapusBanner', utilSim.checkUserSession, (req, res, next) => {
    fs.unlink(req.body.pathLama, function(err, result) {
        if(err) console.log('error', err);
    });
    utilSim.httpSimDelete('/systems/carousel/'+parseInt(req.body.id), req, function(result){
        if(result.rc == "00"){
            res.json({errors: false, success :true, message: result.rd})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
    });
});

router.get('/listBank',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/bank', req, res, function(result){
        if(result.rc == "00"){
            res.json({errors: false, success :true, data:result.data, message: result.rd})
        }else{
            res.json({errors: true, success :false, data:result.data, message: result.rd})
        }
    })
});

router.post('/updateBank', utilSim.checkUserSession, (req, res, next) => {
    req.check('kodeBank', 'invalid Bank').not().isEmpty().withMessage('Bank Harus Dipilih');
    req.check('noRek', 'invalid noRek').not().isEmpty().withMessage('Nomor Rekening Harus Diisi');
    req.check('atasNama', 'invalid Atas Nama').not().isEmpty().withMessage('Atas Nama Harus Diisi');
    req.check('cabang', 'invalid Cabang').not().isEmpty().withMessage('Cabang Harus Diisi');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : parseInt(req.body.id),
            kodeBank: req.body.kodeBank,
            atasNama: req.body.atasNama,
            noRek: req.body.noRek,
            cabang: req.body.cabang
        });
        utilSim.httpSimPut('/systems/users/bank/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: false, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/updateIdentitas', utilSim.checkUserSession, (req, res, next) => {    
    uploadKtp(req, res, function(err) {
        req.checkBody('noTelp', 'invalid No Telp').not().isEmpty().withMessage('No Telp Harus diisi')
        req.checkBody('firstName', 'invalid Nama').not().isEmpty().withMessage('Nama Sesuai KTP harus diisi');
        req.checkBody('email', 'invalid Email').not().isEmpty().withMessage('Email harus diisi');
        req.checkBody('alamat', 'invalid Detail Alamat').not().isEmpty().withMessage('Detail harus diisi');
        req.checkBody('gender', 'invalid Gender').not().isEmpty().withMessage('Gender harus diisi');
        req.checkBody('tempatLahir', 'invalid Tempat Lahir').not().isEmpty().withMessage('Tempat Lahir harus diisi');
        req.checkBody('tanggalLahir', 'invalid Tanggal Lahir').not().isEmpty().withMessage('Tanggal Lahir harus diisi');
        req.checkBody('nik', 'invalid NIK').not().isEmpty().withMessage('NIK harus diisi');

        var errors = req.validationErrors();
        if(errors){
            res.json({errors: errors})
        }else{
            let userInput;
            if (err) {
                return res.send({ success :false, message: err.message});
            }
            
            if(req.file !== undefined){
                userInput = JSON.stringify({
                    id : parseInt(req.body.id),
                    name: req.body.firstName,
                    nik: req.body.nik,
                    email: req.body.email,
                    noTelp: req.body.noTelp,
                    alamatKtp: req.body.alamat,
                    gender: req.body.gender,
                    tempatLahir: req.body.tempatLahir,
                    tanggalLahir: req.body.tanggalLahir,
                    instagram: req.body.instagram,
                    facebook: req.body.facebook,
                    roleId: parseInt(req.body.role),
                    pathKtp: req.file.path,
                });
                fs.unlink(req.body.pathLama, function(err, result) {
                    if(err) console.log('error', err);
                });
            }else{
                userInput = JSON.stringify({
                    id : parseInt(req.body.id),
                    name: req.body.firstName,
                    nik: req.body.nik,
                    email: req.body.email,
                    noTelp: req.body.noTelp,
                    alamatKtp: req.body.alamat,
                    gender: req.body.gender,
                    instagram: req.body.instagram,
                    facebook: req.body.facebook,
                    tempatLahir: req.body.tempatLahir,
                    tanggalLahir: req.body.tanggalLahir,
                    roleId: parseInt(req.body.role),
                });
            }
            
            utilSim.httpSimPut('/systems/users/identitas', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: false, success :true, message: result.rd})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });  
        }          
    }) 
});

module.exports = router;
