const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/koperasiPinjaman', { title: "Koperasi Pinjaman", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/koperasi/pinjaman?page='+req.query.page+"&limit="+req.query.limit, req, res, function(result){
        
        let jumlahPage = Math.round(result.total / req.query.limit)
        res.json({success :true, data: result, jumlahData: jumlahPage})
    })
});

router.get('/detail',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/koperasi/pinjaman/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('Keterangan harus diisi!')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            pinjamanId: req.body.id,
            status: parseInt(req.body.status),
            keterangan: req.body.keterangan
        });

        utilSim.httpSimPut('/systems/koperasi/pinjaman/status/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;
