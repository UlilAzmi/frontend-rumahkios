const express = require('express');
const utilSim = require('../core/utilSim');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/absensiHarian', req, res, function(result){
        res.render('absensiHarian', {data:result.data, title: "Absensi Harian", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user)});
    })
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/absensiHarian/data?bulan=02-2020', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/waliKelas/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('wali', 'invalid user wali').not().isEmpty().withMessage('User wali required')
    req.check('kelas', 'invalid kelas').not().isEmpty().withMessage('kelas is required');
    req.check('tahun', 'invalid tahun').not().isEmpty().withMessage('Tahun ajaran is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            wali : {id : parseInt(req.body.wali) } ,
            kelas : {id : parseInt(req.body.kelas) } ,
            tahun : {id : parseInt(req.body.tahun) },
            keterangan: req.body.keterangan,  
        });
        utilSim.httpSimPost('/systems/waliKelas', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/waliKelas/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('wali', 'invalid user wali').not().isEmpty().withMessage('User wali required')
    req.check('kelas', 'invalid kelas').not().isEmpty().withMessage('kelas is required');
    req.check('tahun', 'invalid tahun').not().isEmpty().withMessage('Tahun ajaran is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            wali : {id : parseInt(req.body.wali) } ,
            kelas : {id : parseInt(req.body.kelas) } ,
            tahun : {id : parseInt(req.body.tahun) },
            keterangan: req.body.keterangan,  
        });

        utilSim.httpSimPut('/systems/waliKelas/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;