const express = require('express');
const utilSim = require('../../core/utilSim');
const e = require('express');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/transaksi', {data:null, title: "Transaksi", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/myTransaksi',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        let menu = utilSim.getMenuDevel(); 
        let user = utilSim.getUserDevel();
        res.render('admin/myTransaksi', {title: "MY Transaksi", menu: JSON.parse(menu), user: JSON.parse(user), layout: "admin", env: "devel"});
    }else{
        res.render('admin/myTransaksi', {title: "MY Transaksi", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});   
    }
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    let lunas = '&lunas=' + req.query.lunas;
    let via = req.query.via;
    let tanggal = req.query.tanggal;
    let tanggalBeli = req.query.tanggalBeli;
    
    if(req.query.lunas == undefined){
        lunas = '';
    }
    if(via == undefined){
        via = null;
    }
    if(tanggal == undefined){
        tanggal = null;
    }
    if(tanggalBeli == undefined){
        tanggalBeli = null;
    }
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/transaksi?date=' + param, req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }else{
        utilSim.httpSimGet('/systems/transaksi?tanggal=' + tanggal + lunas + '&via=' + via + '&tanggalBeli=' + tanggalBeli + '&jenisTransaksi=' + req.query.jenisTransaksi 
        + '&page='+req.query.page +'&limit=' + req.query.limit + '&transaksiId=' + req.query.transaksiId, req, res, function(result){
            let jumlahPage = Math.round(result.total / req.query.limit)
            res.json({success :true, data: result.data, jumlahData: result.total, jumlahPage: jumlahPage})
        })
    }
});

router.get('/myTransaksi1',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/transaksi/myTransaksi', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }else{
        utilSim.httpSimGet('/systems/transaksi/myTransaksi', req, res, function(result){
            res.json({success :true, data: result.data})
        })
    }
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/' + req.query.id, req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, data: result.rd})
        }
    })
});

router.get('/detail',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/detail/' + req.query.id, req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, data: result.rd})
        }
    })
});

router.get('/cek-biller',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/transaksi/checkStatus/' + req.query.id, req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, data: result.rd})
        }
    })
});

router.get('/editMyTransaksi',utilSim.checkUserSession, (req, res, next) => {
    let env = req.query.env;
    if(undefined != env && env == 'devel'){
        utilSim.httpSimGetDevel('/systems/transaksi/myTransaksi/' + req.query.id, req, res, function(result){
            if(result.data != null){
                res.json({success :true, data: result.data[0]})
            }else{
                res.json({success :false, data: result.rd})
            }
        })
    }else{
        utilSim.httpSimGet('/systems/transaksi/myTransaksi/' + req.query.id, req, res, function(result){
            if(result.data != null){
                res.json({success :true, data: result.data[0]})
            }else{
                res.json({success :false, data: result.rd})
            }
        })
    }
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('via', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('tagihans', 'invalid tagihan').not().isEmpty().withMessage('Tagihan is required')
    
    var errors = req.validationErrors();
    var tagihan = [];
    
    for(a = 0 ; a < req.body.tagihans.length ; a++){
        tagihan.push({"id" : req.body.tagihans[a]})
    }
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            jenisTransaksi: req.body.jenisTransaksi,
            via: req.body.via,
            tagihans : tagihan ,
            linkIcon: req.body.icon
        });
        utilSim.httpSimPost('/systems/transaksi/add', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd, data: result.data})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/sukses', utilSim.checkUserSession, (req, res, next) => {
    req.check('idTransaksi', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    
    var errors = req.validationErrors();
    
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id: req.body.idTransaksi,
            keterangan: '',
            status: 1 //status sukses static 1
        });
        
        utilSim.httpSimPost('/systems/transaksi/suksesRefund', userInput, req, function(result){
            res.json({errors: errors, success :true, message: result.data.message, data: result.data})
        });
        
        // sukses manual versi lama
        // utilSim.httpSimPost('/systems/transaksi/sukses', userInput, req, function(result){
        //     if(result == "ACCEPTED"){
        //         res.json({errors: errors, success :true, message: result.rd, data: result})
        //     }else{
        //         res.json({errors: true, success :false, message: result.rd})
        //     }
        // });
    }
});

router.post('/refund', utilSim.checkUserSession, (req, res, next) => {
    req.check('idTransaksi', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    req.check('keterangan', 'invalid data').not().isEmpty().withMessage('keterangan harus diisi')
    
    var errors = req.validationErrors();
    
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id: req.body.idTransaksi,
            keterangan: req.body.keterangan,
            status: 2 //status refund static 2
        });
        
        utilSim.httpSimPost('/systems/transaksi/suksesRefund', userInput, req, function(result){
            res.json({errors: errors, success :true, message: result.data.message, data: result.data})
        });
    }
});

router.post('/inquiry', utilSim.checkUserSession, (req, res, next) => {
    req.check('via', 'invalid nama').not().isEmpty().withMessage('Name is required')
    
    var errors = req.validationErrors();
    
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            via: req.body.via,
            jenisTransaksi: 1,
            linkIcon: req.body.icon,
            idProduk: parseInt(req.body.idProduk),
        });
        utilSim.httpSimPost('/systems/transaksi/inquiry', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd, data: result.data})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/productInquiry', utilSim.checkUserSession, (req, res, next) => {
    req.check('via', 'invalid Payment Method').not().isEmpty().withMessage('Metode pembayaran harus diisi')
    
    var errors = req.validationErrors();
    
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            idProduk: parseInt(req.body.idProduk),
            idCart: parseInt(req.body.idCart),
            jenisTransaksi: 2,
            via: req.body.via
        });

        utilSim.httpSimPost('/systems/transaksi/inquiry', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd, data: result.data})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/inquiryTagihan', utilSim.checkUserSession, (req, res, next) => {
    req.check('via', 'invalid nama').not().isEmpty().withMessage('Name is required')
    
    var errors = req.validationErrors();
    
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            via: req.body.via,
            jenisTransaksi: req.body.jenisTransaksi,
            tagihanId: parseInt(req.body.tagihanId),
        });
        utilSim.httpSimPost('/systems/transaksi/inquiry', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd, data: result.data})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});


router.post('/productCartInquiry', utilSim.checkUserSession, (req, res, next) => {
    req.check('via', 'invalid Payment Method').not().isEmpty().withMessage('Metode pembayaran harus diisi')
    req.check('idProvinsi', 'Provinsi tidak sesuai').not().isEmpty().withMessage('Provinsi harus diisi')
    req.check('idKota', 'kota tidak sesuai').not().isEmpty().withMessage('Kota harus diisi')
    req.check('idKecamatan', 'kecamatan tidak sesuai').not().isEmpty().withMessage('Kecamatan harus diisi')
    req.check('detailAlamat', 'detail alamat tidak sesuai').not().isEmpty().withMessage('detail alamat harus diisi')
    
    var errors = req.validationErrors();
    
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            idProduk: parseInt(req.body.idProduk),
            idCart: parseInt(req.body.idCart),
            jenisTransaksi: 2,
            via: req.body.via
        });
        let env = req.query.env;
        if(undefined != env && env == 'devel'){
            utilSim.httpSimPostDevel('/systems/transaksi/inquiry', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: errors, success :true, message: result.rd, data: result.data})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });
        }else{
            utilSim.httpSimPost('/systems/transaksi/inquiry', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: errors, success :true, message: result.rd, data: result.data})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });
        }
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/transaksi/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('nominal', 'invalid nominal').not().isEmpty().withMessage('nominal is required');
    req.check('group', 'invalid group').not().isEmpty().withMessage('group is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            nama: req.body.nama,
            keterangan: req.body.keterangan,
            jurusan : {id : req.body.jurusan},
            kelasTingkat : {id : req.body.tingkat},
            kelas : {id : req.body.kelas} ,  
            nominal :  parseInt(req.body.nominal) ,
            groupProduct : {id : req.body.group} ,
        });

        utilSim.httpSimPut('/systems/transaksi/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;