const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/notifWa', {title: "Notifikasi Whatsapp", 
    menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/notifWa', req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data})
        }else{
            res.json({success :false, message: result.rd}) 
        }
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/notifWa/' + req.query.id, req, res, function(result){
        if(result.rc == "00"){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, message: result.rd}) 
        }
    })
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    let userInput = JSON.stringify({
        id : req.body.id
    });

    utilSim.httpSimPost('/systems/notifWa', userInput, req, function(result){
        if(result.rc == "00"){
            res.json({errors: false, success :true, message: result.rd})
        }else{
            res.json({errors: true, success :false, message: result.rd})
        }
    });
});

module.exports = router;