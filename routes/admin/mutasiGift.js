const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/mutasiGift', {data:null, title: "Mutasi Gift", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/mutasiGift', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/mutasiGift/' + req.query.id, req, res, function(result){
        if(result.data != null){
            res.json({success :true, data: result.data[0]})
        }else{
            res.json({success :false, data: result.rd})
        }
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('via', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('tagihans', 'invalid tagihan').not().isEmpty().withMessage('Tagihan is required')
    
    var errors = req.validationErrors();
    var tagihan = [];
    
    for(a = 0 ; a < req.body.tagihans.length ; a++){
        tagihan.push({"id" : req.body.tagihans[a]})
    }
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            jenisTransaksi: req.body.jenisTransaksi,
            via: req.body.via,
            tagihans : tagihan ,
            linkIcon: req.body.icon
        });
        utilSim.httpSimPost('/systems/transaksi/add', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd, data: result.data})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/transaksi/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('nominal', 'invalid nominal').not().isEmpty().withMessage('nominal is required');
    req.check('group', 'invalid group').not().isEmpty().withMessage('group is required');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            nama: req.body.nama,
            keterangan: req.body.keterangan,
            jurusan : {id : req.body.jurusan},
            kelasTingkat : {id : req.body.tingkat},
            kelas : {id : req.body.kelas} ,  
            nominal :  parseInt(req.body.nominal) ,
            groupProduct : {id : req.body.group} ,
        });

        utilSim.httpSimPut('/systems/transaksi/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;