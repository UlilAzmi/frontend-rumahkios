const express = require('express');
const utilSim = require('../../core/utilSim');
const multer = require('multer');
var moment = require('moment');
const fs = require('fs');
const { min } = require('moment');
const router = express.Router();

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/datas/product");
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + "_" + file.originalname);
    },
});

const upload = multer({
    storage: Storage,
    limits: 1000000,
    /*dest: 'datas',*/
    fileFilter(req, file, cb){
        if(file.originalname.match(/\.(jpeg)$/)){
            file.mimetype = '.jpeg'
        }
	else if(file.originalname.match(/\.(jpg)$/)){
            file.mimetype = '.jpg'
        }else if(file.originalname.match(/\.(png)$/)){
            file.mimetype = '.png'
        }else{
            return cb(new Error('only .jpeg file type'))
        }
        
        cb(undefined, true)
    },
}).single("file");

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/product', {title: "Product", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: 'admin'});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/product', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/product/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});


router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    upload(req, res, function(err) {
        req.checkBody('nama', 'invalid nama').not().isEmpty().withMessage('nama harus diisi')
        req.checkBody('view', 'invalid view').not().isEmpty().withMessage('Jumlah view harus diisi')
        req.checkBody('harga', 'invalid harga jual').not().isEmpty().withMessage('Harga jual harus diisi')
        req.checkBody('untilBuyDate', 'invalid untilBuyDate').not().isEmpty().withMessage('untilBuyDate harus diisi')
        req.checkBody('hargaHidden', 'invalid hargahidden').not().isEmpty().withMessage('hargaHidden harus diisi')
        req.checkBody('hargaCoret', 'invalid harga coret').not().isEmpty().withMessage('harga coret harus diisi')
        req.checkBody('hargaBeli', 'invalid harga Beli').not().isEmpty().withMessage('harga beli harus diisi')
        req.checkBody('feeBiller1', 'invalid Fee Biller1').not().isEmpty().withMessage('Fee Biller1 harus diisi')
        req.checkBody('feeBiller2', 'invalid Fee Biller2').not().isEmpty().withMessage('Fee Biller2 harus diisi')
        req.checkBody('berat', 'invalid Berat').isNumeric({min: 1}).withMessage('Berat Produk minimal 1 Kg')
        
        var errors = req.validationErrors();
        if(errors){
            res.json({errors: errors})
        }else{
            if (err) {
                return res.send({ success :false, message: err.message});
            }

            let userInput = JSON.stringify({
                nama: req.body.nama,
                view: req.body.view,
                isFlashsale: req.body.fSale,
                isActive: req.body.active,
                untilBuyDate: req.body.untilBuyDate,
                untilBuyTime: req.body.untilBuyTime + ":00",
                harga: req.body.harga,
                hargaHidden: req.body.hargaHidden,
                hargaCoret: req.body.hargaCoret,
                image: req.file.path,
                hargaBeli: req.body.hargaBeli,
                feeBiller1: req.body.feeBiller1,
                feeBiller2: req.body.feeBiller2,
                weight: req.body.berat,
                keterangan: req.body.keterangan,
            });
            
            utilSim.httpSimPost('/systems/product', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: false, success :true, message: result.rd})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });   
        }
    })
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/product/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                fs.unlink(req.body.path, function(err, result) {
                    if(err) console.log('error', err);
                });
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    upload(req, res, function(err) {
        req.checkBody('nama', 'invalid nama').not().isEmpty().withMessage('nama harus diisi')
        req.checkBody('view', 'invalid view').not().isEmpty().withMessage('Jumlah view harus diisi')
        req.checkBody('untilBuyDate', 'invalid untilBuyDate').not().isEmpty().withMessage('untilBuyDate harus diisi')
        req.checkBody('hargaHidden', 'invalid hargahidden').not().isEmpty().withMessage('hargaHidden harus diisi')
        req.checkBody('hargaCoret', 'invalid harga coret').not().isEmpty().withMessage('harga coret harus diisi')
        req.checkBody('hargaBeli', 'invalid harga Beli').not().isEmpty().withMessage('harga beli harus diisi')
        req.checkBody('feeBiller1', 'invalid Fee Biller1').not().isEmpty().withMessage('Fee Biller1 harus diisi')
        req.checkBody('feeBiller2', 'invalid Fee Biller2').not().isEmpty().withMessage('Fee Biller2 harus diisi')
        req.checkBody('berat', 'invalid Berat').isNumeric({min: 1}).withMessage('Berat Produk minimal 1 Kg')

        var errors = req.validationErrors();
        if(errors){
            res.json({errors: errors})
        }else{
            let userInput;
            waktu = req.body.untilBuyTime;
            if(waktu.split(":").length == 2){
                waktu = req.body.untilBuyTime + ":00";
            }
            if(req.file !== undefined){
                //b64 = 'data:image/jpeg;base64,' + new Buffer(fs.readFileSync(req.file.path)).toString("base64");
                userInput = JSON.stringify({
                    id: parseInt(req.body.id),
                    nama: req.body.nama,
                    view: req.body.view,
                    isFlashsale: req.body.fSale,
                    isActive: req.body.active,
                    hargaHidden: req.body.hargaHidden,
                    hargaCoret: req.body.hargaCoret,
                    untilBuyDate: req.body.untilBuyDate,
                    untilBuyTime: waktu,
                    image: req.file.path,
                    hargaBeli: req.body.hargaBeli,
                    feeBiller1: req.body.feeBiller1,
                    feeBiller2: req.body.feeBiller2,
                    weight: req.body.berat,
                    keterangan: req.body.keterangan,
                });
                fs.unlink(req.body.pathLama, function(err, result) {
                    if(err) console.log('error', err);
                });
            }else{
                userInput = JSON.stringify({
                    id: parseInt(req.body.id),
                    nama: req.body.nama,
                    view: req.body.view,
                    isFlashsale: req.body.fSale,
                    isActive: req.body.active,
                    hargaHidden: req.body.hargaHidden,
                    hargaCoret: req.body.hargaCoret,
                    untilBuyDate: req.body.untilBuyDate,
                    untilBuyTime: waktu,
                    hargaBeli: req.body.hargaBeli,
                    feeBiller1: req.body.feeBiller1,
                    feeBiller2: req.body.feeBiller2,
                    weight: req.body.berat,
                    keterangan: req.body.keterangan,
                });
            }
            
            utilSim.httpSimPut('/systems/product/', userInput, req, function(result){
                if(result.rc == "00"){
                    res.json({errors: false, success :true, message: result.rd})
                }else{
                    res.json({errors: true, success :false, message: result.rd})
                }
            });
        }            
    })
});

module.exports = router;
