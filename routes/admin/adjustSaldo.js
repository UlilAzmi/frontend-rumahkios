const express = require('express');
const utilSim = require('../../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('admin/adjustSaldo', { title: "Adjust Saldo", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user), layout: "admin"});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/adjustSaldo?page='+req.query.page+'&limit='+req.query.limit+'&filter=&userAdjust=' + req.query.userTujuan, req, res, function(result){
        let jumlahPage = Math.round(result.total / req.query.limit)
        res.json({success :true, data: result.data, jumlahData: result.total, jumlahPage: jumlahPage})
    })
});


router.post('/adjust', utilSim.checkUserSession, (req, res, next) => {
    req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('Keterangan tidak boleh kosong!')
    req.check('nominal', 'invalid nominal').not().isEmpty().withMessage('nominal tidak boleh kosong!')
    req.check('userAsal', 'invalid user asal').not().isEmpty().withMessage('User Asal tidak boleh kosong!')
    req.check('userTujuan', 'invalid user tujuan').not().isEmpty().withMessage('User Tujuan tidak boleh kosong!')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            userAsal : parseInt(req.body.userAsal),
            userTujuan : parseInt(req.body.userTujuan),
            nominal : parseInt(req.body.nominal),
            type: req.body.type,
            keterangan: req.body.keterangan 
        });
        
        utilSim.httpSimPost('/systems/transaksi/adjustSaldo/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;