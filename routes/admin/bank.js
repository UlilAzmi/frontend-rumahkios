const express = require('express');
const utilSim = require('../core/utilSim');
const router = express.Router();


router.get('/', utilSim.checkUserSession, (req, res, next) => {
    res.render('bank', { title: "Bank", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user)});
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/bank', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/bank/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('admin', 'invalid admin').not().isEmpty().withMessage('Admin is required')
    req.check('bankId', 'invalid Bank ID').not().isEmpty().withMessage('Kode Bank is required')
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            nama: req.body.nama,
            bankId: req.body.bankId,
            biayaAdmin: req.body.admin,
            keterangan: req.body.keterangan,
        });
        
        utilSim.httpSimPost('/systems/bank', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')
    console.log(req.body.id)
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/bank/' + req.body.id, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('admin', 'invalid admin').not().isEmpty().withMessage('Biaya Admin is required')
    req.check('bankId', 'invalid Bank ID').not().isEmpty().withMessage('Kode Bank is required')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            nama: req.body.nama,
            bankId: req.body.bankId,
            biayaAdmin: req.body.admin,
            keterangan: req.body.keterangan
        });

        utilSim.httpSimPut('/systems/bank/', userInput, req, function(result){
            console.log(result)
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;