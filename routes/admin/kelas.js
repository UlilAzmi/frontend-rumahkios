const express = require('express');
const utilSim = require('../core/utilSim');
const Kelas = require('../core/c_kelas');
const router = express.Router();

const kelas = new Kelas();

router.get('/',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/kelas', req, res, function(result){
        res.render('kelas', {data:result.data, title: "Kelas", menu: JSON.parse(req.session.menu), user: JSON.parse(req.session.user)});
    })
});

router.get('/all',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/kelas', req, res, function(result){
        res.json({success :true, data: result.data})
    })
});

router.get('/edit',utilSim.checkUserSession, (req, res, next) => {
    utilSim.httpSimGet('/systems/kelas/' + req.query.id, req, res, function(result){
        res.json({success :true, data: result.data[0]})
    })
});

router.post('/add', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');
    req.check('tingkat', 'invalid tingkat').not().isEmpty().withMessage('tingkat is required');
    req.check('jurusan', 'invalid jurusan').not().isEmpty().withMessage('jurusan is required');
    
    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            nama: req.body.nama,
            keterangan: req.body.keterangan,
            jurusan : {id : parseInt(req.body.jurusan) } ,
            tingkat : {id : parseInt(req.body.tingkat) }  
        });
        utilSim.httpSimPost('/systems/kelas', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: true, success :false, message: result.rd})
            }
        });
    } 
});

router.post('/delete', utilSim.checkUserSession, (req, res, next) => {
    req.check('id', 'invalid data').not().isEmpty().withMessage('Invalid Data')

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        utilSim.httpSimDelete('/systems/kelas/' + req.body.id, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    }
});

router.post('/update', utilSim.checkUserSession, (req, res, next) => {
    req.check('nama', 'invalid nama').not().isEmpty().withMessage('Name is required')
    req.check('keterangan', 'invalid keterangan').not().isEmpty().withMessage('keterangan is required');
    req.check('tingkat', 'invalid tingkat').not().isEmpty().withMessage('tingkat is tingkat');
    req.check('jurusan', 'invalid jurusan').not().isEmpty().withMessage('jurusan is jurusan');

    var errors = req.validationErrors();
    if(errors){
        res.json({errors: errors})
    }else{
        let userInput = JSON.stringify({
            id : req.body.id,
            nama: req.body.nama,
            keterangan: req.body.keterangan,
            jurusan : {id : parseInt(req.body.jurusan) } ,
            tingkat : {id : parseInt(req.body.tingkat) }  
        });

        utilSim.httpSimPut('/systems/kelas/', userInput, req, function(result){
            if(result.rc == "00"){
                res.json({errors: errors, success :true, message: result.rd})
            }else{
                res.json({errors: errors, success :false, message: result.rd})
            }
        });
    } 
});

module.exports = router;