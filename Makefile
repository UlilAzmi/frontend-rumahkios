# A Self-Documenting Makefile: http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
RUMAHKIOS_FE_API_REGISTRY ?="docker.io/akooe32/frontend-rumahkios"
VERSION=version-fe-staging.txt
OLD_VER=$(shell sed -n 2p $(VERSION))
NEW_VER=$(shell expr $(OLD_VER) + 1)
IMAGE_TAG=$(RUMAHKIOS_FE_API_REGISTRY):staging-v1.$(NEW_VER)

.PHONY: build-image-staging
build-image-staging:
        @DOCKER_BUILDKIT=1 docker build -t $(IMAGE_TAG) .
        @echo "done building docker image for staging"

.PHONY: push-image-staging
push-image-staging:
        @docker push $(IMAGE_TAG)
        @sed -i '2s/'"$(OLD_VER)"'/'"$(NEW_VER)"'/' $(VERSION)
	@echo "done push image to docker registry"

.PHONY: build-all-staging
build-all-staging: build-image-staging push-image-staging
        @echo "done building docker image for RumahKios Frontend!"

