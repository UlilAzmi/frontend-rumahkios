FROM node:lts-alpine3.13 as build
WORKDIR /app
RUN chown -R node:node /app
COPY . .
USER node
RUN npm install
COPY --chown=node:node . .
EXPOSE 8080
CMD [ "node", "app.js"
