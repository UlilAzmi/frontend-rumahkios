#!/bin/bash
# echo "Current WD: $PWD";

if [ -z "$1" ]; then
    echo "[*] Run supervisor with 2 argument. e.g: $PWD/spv.sh [command]";
    exit 1;
fi

run_file="$PWD/public/whatsapp";
if [ "$1" = "stop" ]; then
    list_pid=$(ps aux | grep $run_file | grep -v grep | awk '{print $2}');
    if [ ! -z "$list_pid" ]; then
        while read -r pid; do
            echo "[*] Stopping pid $pid";
            kill -9 $pid
        done <<< "$list_pid"
    fi
elif [ "$1" = "start" ]; then
    list_pid=$(ps aux | grep $run_file | grep -v grep | awk '{print $2}');
    if [ ! -z "$list_pid" ]; then
        while read -r pid; do
            echo "[*] Stopping pid $pid";
            kill -9 $pid
        done <<< "$list_pid"
    fi
    $run_file &
elif [ "$1" = "show" ]; then
    ps aux | grep $run_file | grep -v grep;
else
    echo "[*] Available command: show, stop, start";
    exit 2;
fi
